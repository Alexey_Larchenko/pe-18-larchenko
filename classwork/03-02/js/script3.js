class WhackAMoleGame {
    constructor() {
        this._difficulty = 0;
        this._playerCells = 0;
        this._computerCells = 0;
        this._arrayOfCells = [];
        this._counter = 0;
        this._levels = [['Easy', 1500], ['Medium', 1000], ['Hard', 500]];

        this.chooseDifficulty(this.startTheGame.bind(this));
    }

    chooseDifficulty(startTheGame) {
        const buttonsContainer = document.createElement('div');
        document.body.prepend(buttonsContainer);

        this._levels.map((el) => {
            buttonsContainer.insertAdjacentHTML('afterbegin', `<button id="${el[0]}">${el[0]}</button>`)
        });

        this.chooseDifficultyHandler(startTheGame, this._levels, buttonsContainer, this._difficulty);
    }

    chooseDifficultyHandler(startTheGame, levelsName, parent) {
        document.body.addEventListener('click', function speedListener(event) {
            let difficultyLevel = 0;
            let targetName = event.target.id;

            levelsName.forEach(function (elem) {
                if (elem.includes(targetName)) {
                    difficultyLevel = targetName;
                    document.body.removeEventListener('click', speedListener);
                    startTheGame(difficultyLevel);
                    parent.remove();
                }
            });
        });
    }

    startTheGame(difficultyLevel) {
        this.setDifficulty(difficultyLevel);
        this.createField();
        this.getNodesOfCells();
        this.makeArrayFromNodeList();
        this.shuffleCellsForGame(this._arrayOfCells);
        this.createCellInGame();
    }

    setDifficulty(value) {
        this._levels.forEach(elem => {
            if (value === elem[0]) {
                this._difficulty = elem[1];
            }
        });
    }

    createField() {
        if (!document.getElementById('fieldContainer')) {
            const fieldContainer = document.createElement('div');
            fieldContainer.id = 'fieldContainer';
            document.body.append(fieldContainer);
        }

        for (let i = 0; i < 100; i++) {
            document.getElementById("fieldContainer")
                .appendChild(document.createElement("td"))
                .classList.add('default-cell');
        }
    }

    getNodesOfCells() {
        this._arrayOfCells = document.querySelectorAll('.default-cell');
    }

    makeArrayFromNodeList() {
        this._arrayOfCells = Array.from(this._arrayOfCells);
    }

    shuffleCellsForGame(arr) {
        for (let i = arr.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [arr[i], arr[j]] = [arr[j], arr[i]];
        }
    }

    createCellInGame() {
        this.showScore();
        this.doWeHaveWinner();
        this._randomCell = this._arrayOfCells[this._counter++];
        if (this._randomCell) {
            this._randomCell.classList.add('in-WhackAMoleGame');
            this.clickListener();
            setTimeout(() => {
                this.createCellInGame();
            }, this._difficulty);
        }
    }

    clickListener() {
        this._ifNoClick = setTimeout(this.giveCellToComputer.bind(this), this._difficulty);
        this._randomCell.addEventListener('click', () => {
            this.giveCellToUser();
            clearTimeout(this._ifNoClick);
        }, {once: true});
    }

    giveCellToUser() {
        this._randomCell.classList.remove('in-WhackAMoleGame');
        this._randomCell.classList.add('player-cell');
        this._playerCells++;
    }

    giveCellToComputer() {
        this._randomCell.classList.remove('in-WhackAMoleGame');
        this._randomCell.classList.add('computer-cell');
        clearTimeout(this._ifNoClick);
        this._computerCells++;
    }

    showScore() {
        if (document.querySelector('.score')) {
            document.querySelector('.score').remove();
        }

        const ul = document.createElement('ul');
        const playerScore = document.createElement('li');
        const computerScore = document.createElement('li');

        ul.classList.add('score');
        ul.id = 'score';

        playerScore.innerText = `Player score: ${this._playerCells}`;
        computerScore.innerText = `Computer score: ${this._computerCells}`;

        document.body.append(ul);
        ul.append(playerScore);
        ul.append(computerScore);
    }

    doWeHaveWinner() {
        if (this._playerCells === 50 || this._computerCells === 50) {
            const winnerName = (this._playerCells > this._computerCells) ? 'player' : 'computer';
            alert(`Winner is ${winnerName}`);
            this.deleteGame();

            const WhackAMoleGam = new WhackAMoleGame;
        }

    }

    deleteGame() {
        document.getElementById('score').remove();
        document.getElementById('fieldContainer').remove();
        this._arrayOfCells = [];
    }
}

const WhackAMoleGam = new WhackAMoleGame();