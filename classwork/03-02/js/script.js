let product = {};
Object.defineProperty(product, 'name', {
   value: 'Laptop',
    configurable: true,
    writable: false,
    enumerable: true
});
Object.defineProperty(product, 'price', {
    value: 1200,
    configurable: true,
    writable: false,
    enumerable: true
});
product.price = 1000;
delete product.name;
console.log(product);