load(1);

function handleNewItems({names, hasMore}) {
    addItems(names);
    handleEnd(!hasMore);
}

function load(page, callback) {
    const req = new XMLHttpRequest();
    req.open('GET', `./data${page}.json`);
    req.onload = function () {
        if (req.status === 200) {
            callback(JSON.parse(req.response));
        }
    };
    req.send();
}

function addItems(items) {
    const parent = document.querySelector('.list');
    const {names} = JSON.parse(req.response);
    names.forEach(name => {
        const item = document.createElement('li');
        item.innerText = names;
        parent.append(item);
    });
}
function handleEnd (isEnd) {
    if (isEnd) {
        loadButton.removeEventListener('click', loadMore);
        loadButton.remove();
    }
}