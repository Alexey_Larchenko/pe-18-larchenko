(function() {
    let classmates;
    classmates = [
        {
            name: 'Глушак Андрій'
        },
        {
            name: 'Щепик Ярослав'
        },
        {
            name: 'Козак Микола'
        }
    ];

    const ulWrapper = customDOM.getById('classmate-wrapper');
    setTimeout(function() {
        classmates.forEach(function(person) {
            const li = customDOM.createElement('li');
            li.innerHTML = person.name;
            customDOM.append(ulWrapper, li);
        });

    }, 1000);

})();

setTimeout(function () {
    document.body.style.backgroundColor = "red";
}, 2000);
