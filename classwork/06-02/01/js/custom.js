/**
 * @name customDOM
 * @desc Об'єкт який реалізує логіку роботи з DOM-ом
 * @type {Object}
 **/

const customDOM = {
    getAllElements: function() {
        return document.getElementsByTagName('*');
    },
    getById: function(id) {
        return document.getElementById(id);
    },
    createElement: function (tagName) {
        return document.createElement(tagName);
    },
    append: function(parent, child) {
        return parent.appendChild(child);
    }
};