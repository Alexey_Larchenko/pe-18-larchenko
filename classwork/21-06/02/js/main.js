const user = {
    name: "Test",
    getName: () => {
        return this.name;
    }
};

console.log(user.getName());

const listComponent = {
    list: [],
    loading: false,
    loadFilms(onLoaded) {
        this.loading = true;
        setTimeout((response) => {
            this.list = ["Film1", "Film2"];
            this.loading = false;
            onLoaded();
        }, 1000);
    }
}

renderFilms(listComponent.list);
listComponent.loadFilms(() => {
    renderFilms.apply(listComponent.list);
});

function renderFilms() {
    console.log(list);
}

function f(a) {
    return (b) => {
        return a + b;
    }
}
const f2 = f(3);
console.log(f2(5));