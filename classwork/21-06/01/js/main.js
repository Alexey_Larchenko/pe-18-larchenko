function createUser() {
    const name = 'Name';
    const age = 22;
    const prop = prompt('prop name: ');
    const val = prompt('value: ');
    const otherUser = {
        name: 'other',
        legs: 2
    };
    return {
        ...otherUser,
        name,
        age,
        children: [{name: 'Child1', age: 10}, {name: 'Child2', age: 7}],
        [prop]: val
    };
}
const user = createUser();
const {name, age: num, children: [,{age}]} = user;
console.log(user);
console.log(age);



function sum({x = 0, y = 0} = {}, {x: x2 = 0, y: y2 = 0} = {}) {
    return {
        x: x + x2,
        y: y + y2
    }
}

console.log(sum({x: 1}, {x: 3, y: -5}));