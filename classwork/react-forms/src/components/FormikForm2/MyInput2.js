import React from 'react';

function MyInput(props) {
    const {form, field, ...rest} = props;
    const {name} = field;

    return (
        <div>
            <input {...field} {...rest} />
            {
                form.touched[name] && form.errors[name] && (
                    <span className='error'>{form.errors[name]}</span>
                )
            }
        </div>
    )
}

export default MyInput
