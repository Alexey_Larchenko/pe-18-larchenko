import React from 'react'
import MyInput from './MyInput';
import {Form, Field, Formik} from 'formik';
import * as yup from 'yup';

const formSchema = yup.object().shape({
    login: yup
        .string()
        .required("This field is required")
        .email("Please enter a valid email"),
    password: yup
        .string()
        .required("This field is required")
        .min(8, "Minimum length is 8 symbols"),
    repeatPassword: yup
        .string()
        .required("This field is required")
        .min(8, "Minimum length is 8 symbols")
})

const customValidation = ({password, repeatPassword}) => {
    const errorMessages = {}

    if (password !== repeatPassword) {
        errorMessages.repeatPassword = 'Passwords do not match'
    }

    return errorMessages;
}

const FormikForm = () => {
    const handleSubmit = (values, {setSubmitting}) => {
        console.log(values);

        setSubmitting(false);
    }

    return (
        <Formik
            initialValues={{
                login: 'admin',
                password: '',
                repeatPassword: ''
            }}
            validate={customValidation}
            validationSchema={formSchema}
            onSubmit={handleSubmit}
        >
            {(formikProps) => {

                return (
                    <Form onSubmit={formikProps.handleSubmit} noValidate>
                        <div>Formik form</div>
                        <Field component={MyInput} type='email' name='login' placeholder='Login'/>
                        <Field component={MyInput} type='password' name='password' placeholder='Password'/>
                        <Field component={MyInput} type='password' name='repeatPassword' placeholder='Repeat password'/>
                        <div>
                            <button type='submit' disabled={formikProps.isSubmitting}>Log in</button>
                            <button type='button' onClick={formikProps.handleReset}>Reset</button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
}

export default FormikForm;