import React, { useState, useEffect } from 'react'

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validateLoginForm = (login, password, repeatPassword, touched) => {
    const errorMessages = {}

    if (!EMAIL_REGEX.test(login) && touched.login) {
        errorMessages.login = 'Please use a valid email'
    }

    if (!login && touched.login) {
        errorMessages.login = 'This field is required'
    }

    if (!password && touched.password) {
        errorMessages.password = 'This field is required'
    }

    if (password !== repeatPassword && touched.repeatPassword) {
        errorMessages.repeatPassword = 'Passwords do not match'
    }

    if (!repeatPassword && touched.repeatPassword) {
        errorMessages.repeatPassword = 'This field is required'
    }

    return errorMessages;
}

const ControlledForm = (props) => {
    const [values, setValues] = useState({
        login: 'admin',
        password: '',
        repeatPassword: ''
    });
    const [errors, setErrors] = useState({});
    const [touched, setTouched] = useState({});

    const submitLoginForm = (e) => {
        e.preventDefault();

        const { login, password, repeatPassword } = values;

        if (Object.keys(errors).length > 0) {
            return;
        }

        console.log(login, password, repeatPassword);
    }

    useEffect(() => {
        const { login, password, repeatPassword } = values;
        const errorMessages = validateLoginForm(login, password, repeatPassword, touched);
        setErrors(errorMessages);
    }, [values, touched])

    const handleChange = (e) => {
        const { value, name } = e.target;
        setValues({ ...values, [name]: value });
    }

    const handleBlur = (e) => {
        const { name } = e.target;
        setTouched({ ...touched, [name]: true });
    }

    return (
        <form onSubmit={submitLoginForm} noValidate>
            <div>
                Controlled form
            </div>
            <div>
                <input type='email' name='login' value={values.login} onChange={handleChange} onBlur={handleBlur} placeholder='Login'></input>
                <span className='error'>{errors.login}</span>
            </div>
            <div>
                <input type='password' name='password' value={values.password} onChange={handleChange} onBlur={handleBlur} placeholder='Password'></input>
                <span className='error'>{errors.password}</span>
            </div>
            <div>
                <input type='password' name='repeatPassword' value={values.repeatPassword} onChange={handleChange} onBlur={handleBlur} placeholder='Repeat password'></input>
                <span className='error'>{errors.repeatPassword}</span>
            </div>
            <div>
                <button type='submit'>Log in</button>
            </div>
        </form>
    )
}

export default ControlledForm;