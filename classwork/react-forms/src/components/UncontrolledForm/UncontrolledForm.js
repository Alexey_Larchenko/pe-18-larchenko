import React, { useRef, useState } from 'react'

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const UncontrolledForm = () => {
    const loginRef = useRef(null);
    const passwordRef = useRef(null);
    const repeatPasswordRef = useRef(null);

    const [errors, setErrors] = useState({});

    const validateLoginForm = (login, password, repeatPassword) => {
        const errorMessages = {}

        if (!EMAIL_REGEX.test(login)) {
            errorMessages.login = 'Please use a valid email'
        }

        if (!login) {
            errorMessages.login = 'This field is required'
        }

        if (!password) {
            errorMessages.password = 'This field is required'
        }

        if (password !== repeatPassword) {
            errorMessages.repeatPassword = 'Passwords do not match'
        }

        if (!repeatPassword) {
            errorMessages.repeatPassword = 'This field is required'
        }

        return errorMessages;
    }

    const submitLoginForm = (e) => {
        e.preventDefault();

        const login = loginRef.current.value
        const password = passwordRef.current.value
        const repeatPassword = repeatPasswordRef.current.value

        const errorMessages = validateLoginForm(login, password, repeatPassword);
        setErrors(errorMessages);

        if (Object.keys(errorMessages).length > 0) {
            return;
        }

        console.log(login, password, repeatPassword);
    }

    return (
        <form onSubmit={submitLoginForm} noValidate>
            <div>
                <input type='email' placeholder='Login' ref={loginRef} defaultValue='admin'/>
                <span className='error'>{errors.login}</span>
            </div>
            <div>
                <input type='password' placeholder='Password' ref={passwordRef}/>
                <span className='error'>{errors.password}</span>
            </div>
            <div>
                <input type='password' placeholder='Repeat password' ref={repeatPasswordRef}/>
                <span className='error'>{errors.repeatPassword}</span>
            </div>
            <div>
                <button type='submit'>Log in</button>
            </div>
        </form>
    )
}

export default UncontrolledForm;