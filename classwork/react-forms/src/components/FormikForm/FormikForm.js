import React from 'react'
import MyInput from './MyInput';
import { Form, Field, Formik, ErrorMessage } from 'formik';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
// const requiredInput = (value) => !value && 'This field is required'
// const isEmail = (value) => !EMAIL_REGEX.test(value) && 'Please use a valid email'
// const passwordsMatch = (value, values) => value !== values.password && 'Passwords do not match'

const validateLoginForm = ({login, password, repeatPassword}) => {
    const errorMessages = {}

    if (!EMAIL_REGEX.test(login)) {
        errorMessages.login = 'Please use a valid email'
    }

    if (!login) {
        errorMessages.login = 'This field is required'
    }

    if (!password) {
        errorMessages.password = 'This field is required'
    }

    if (password !== repeatPassword) {
        errorMessages.repeatPassword = 'Passwords do not match'
    }

    if (!repeatPassword) {
        errorMessages.repeatPassword = 'This field is required'
    }

    return errorMessages;
}

const FormikForm = (props) => {
    // const { submitting, handleSubmit, reset } = props;

    return (
        <Formik
            initialValues={{
                login: 'admin',
                password: '',
                repeatPassword: ''
            }}
            validate={validateLoginForm}
        >
            {(formikProps) => {
                return (
                    <Form onSubmit={formikProps.handleSubmit} noValidate>
                        <div>Formik form</div>
                        <Field component='input' type='email' name='login' placeholder='Login'></Field>
                        <span className='error'><ErrorMessage name='login' /></span>
                        <Field component='input' type='password' name='password' placeholder='Password'></Field>
                        <span className='error'><ErrorMessage name='password' /></span>
                        <Field component='input' type='password' name='repeatPassword' placeholder='Repeat password'></Field>
                        <span className='error'><ErrorMessage name='repeatPassword' /></span>
                        <div>
                            <button type='submit' disabled={formikProps.isSubmitting}>Log in</button>
                            <button type='button' onClick={formikProps.handleReset}>Reset</button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
}

// FormikForm = reduxForm({
//   form: 'login',
//   // validate: validateLoginForm
// })(FormikForm)

export default FormikForm;