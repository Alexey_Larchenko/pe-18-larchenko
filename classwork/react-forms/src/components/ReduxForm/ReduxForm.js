import React from 'react'
import { Field, reduxForm } from 'redux-form';
import MyInput from './MyInput';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const requiredInput = (value) => !value && 'This field is required'
const isEmail = (value) => !EMAIL_REGEX.test(value) && 'Please use a valid email'
const passwordsMatch = (value, values) => value !== values.password && 'Passwords do not match'

let ReduxForm = (props) => {
    // console.log('Redux form', props);
    const { submitting, handleSubmit, reset } = props;

    return (
        <form onSubmit={handleSubmit} noValidate>
            <div>Redux form</div>
            <Field component={MyInput} type='email' name='login' placeholder='Login' validate={[requiredInput, isEmail]}></Field>
            <Field component={MyInput} type='password' name='password' placeholder='Password' validate={[requiredInput]}></Field>
            <Field component={MyInput} type='password' name='repeatPassword' placeholder='Repeat password' validate={[requiredInput, passwordsMatch]}></Field>
            <div>
                <button type='submit' disabled={submitting}>Log in</button>
                <button type='button' onClick={reset}>Reset</button>
            </div>
        </form>
    )
}

ReduxForm = reduxForm({
    form: 'login',
    initialValues: {
        login: 'admin',
        password: '',
        repeatPassword: ''
    },
    // validate: validateLoginForm
})(ReduxForm)

export default ReduxForm;