import React from 'react';

function MyInput(props) {
    const { input, meta, ...rest } = props;

    return (
        <div>
            <input {...input} {...rest} />
            {
                meta.touched && meta.error && (
                    <span className='error'>{meta.error}</span>
                )
            }
        </div>
    )
}

export default MyInput
