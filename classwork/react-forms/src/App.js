import React from 'react';
import './App.css';
import UncontrolledForm from './components/UncontrolledForm/UncontrolledForm';
import ControlledForm from './components/ControlledForm/ControlledForm';
import ReduxForm from './components/ReduxForm/ReduxForm';
import FormikForm from './components/FormikForm/FormikForm';

function App() {
    const handleSubmit = (props) => {
        console.log(props);
    }

    return (
        <div className="App">
            <UncontrolledForm />
            <ControlledForm />
            {/* <ReduxForm onSubmit={handleSubmit} /> */}
            <FormikForm />
        </div>
    );
}

export default App;
