const {src, dest, series, watch} = require('gulp');
const rename = require('gulp-rename');
const del = require('del');
const browserSync = require('browser-sync').create();

function build() {
    return src('./page.html')
        .pipe(rename('index.html'))
        .pipe(dest('./built/'));
}

function cleanBuild() {
    return del('./built');
}

function reloadOnChange() {
    browserSync.init({
        server: './built'
    });
    watch(['./page.html']).on('change',series(build, browserSync.reload));
}

exports.default = series(cleanBuild, build);
exports.dev123 = series(build, reloadOnChange);
