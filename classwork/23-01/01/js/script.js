function init() {
    const PI = 3.14;
    let result = 0;

    console.warn('Some warning message!');
    let userNumber = +prompt('Number', '1');

    result = userNumber * PI;

    return result;
}

console.log(`Result -> ${init()}`);