// GEt, POST, PUT, DELETE, PATCH

// $.ajax('http://danit.com.ua/free-towns-list-json', {
//     method: 'GET',
//     success: (res) => console.log(res),
// })
// document.body.classList.add('loading');
// $.getJSON('http://danit.com.ua/free-towns-list-json')
//     .then(
//         res => console.log(res),
//         err => console.warn(err),
//     )
//     .always(() => {
//         document.body.classList.remove('loading');
//     })
// .done(res => {
//     console.log(res)
// })
// .fail(err => {
//     console.warn(err)
// })

const form = document.querySelector('form');
form.addEventListener('submit', event => {
    event.preventDefault();
    const {target: formEl} = event;
    const data = new FormData(formEl);
    const dataToSend = Object.fromEntries(data.entries());
    $.post(formEl.action, dataToSend)
        .then(res => console.log(res), err => console.warn(err));
});