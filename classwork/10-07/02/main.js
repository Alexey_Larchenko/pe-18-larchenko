class List {
    constructor(selector, dataUrl) {
        this._url = dataUrl;
        this._el = document.querySelector(selector);
        this._initReloadButton();
        this._getData();
    }

    _getData() {
        this._el.classList.add('loading');
        $.getJSON(this._url)
            .done(this._onData.bind(this))
            .always(() => this._el.classList.remove('loading'));
    }

    _onData(users) {
        users.forEach(({name, age}) => {
            const li = document.createElement('li');
            li.innerText = `${name}: ${age}`;
            this._el.append(li);
        });
    }

    _initReloadButton() {
        const button = document.createElement('button');
        button.type = 'button';
        button.innerText = 'Reload';
        button.addEventListener('reload', this._reload.bind(this));
        this._el.after(button);
    }

    _reload() {
        this._getData();
    }
}

const list1 = new List(
    '.list-1',
    'http://dan-it.herokuapp.com/list1'
);

const list2 = new List(
    '.list-2',
    'http://dan-it.herokuapp.com/list2'
);