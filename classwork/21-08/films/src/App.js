import React, {useEffect, useState} from 'react';
import './App.css';
import axios from 'axios';
import Body from "./components/Body/Body";

function App() {

    useEffect(() => {
        axios('https://swapi.dev/api/films/')
            .then(res => console.log(res.data.results));
    })

  return (
    <div className="App">
      <h2>Список фильмов</h2>
        <Body/>
    </div>
  );
}

export default App;
