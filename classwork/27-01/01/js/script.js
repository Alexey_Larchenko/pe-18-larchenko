// function getCar() {
//     let name = prompt('Enter name of the car');
//     let model = prompt('Enter model');
//     let price = prompt('Enter price');
//
//     return {
//         name: name,
//         model: model,
//         price: price
//     }
// }
//
// console.log(getCar());
//

function getUser(userName, userAge) {
    return {
        name: userName,
        age: userAge,
        putAge: function () {
            this.age++;
        },
        addField: function(key, value) {
            this[key] = value;
        }
    };

}

const user = getUser('Alex', 16);
user.addField('mobile', 'samsung');
