const jCurrency = (function () {
    let currentCurrency = 'UAH';

    const rateList = {
        UAH: {
            USD: 24.50,
            EUR: 27.34
        },

        USD: {
            UAH: 0.046,
            EUR: 0.87
        }
    };


    return {
        name: 'jCurrency',
        comment: 'Library to manage the currency stuff',

        getCurrentCurrency: function () {
            return currentCurrency;
        },

        setCurrentCurrency: function (currency) {
            currentCurrency = currency;
        },

        currentCurrencyToUsd: function (quantity = 1) {
            return rateList [ currentCurrency ]['USD'] * quantity;
        }
    };
})();