$(onReady);

function onReady() {
    const list = $('.list-item > li');
    list.each(function (index) {
        $(this)
            .html(`${$(this).html()} +`)
            .css({'color': 'red'});
    });

    const button = $('<button>');
    $(button).text('Hello');
    $('.container').append(button);
}