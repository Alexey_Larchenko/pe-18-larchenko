const num1 = +prompt('First number');
const num2 = +prompt('Second number');
const operation = prompt('Operation');

let result = 0;

switch (operation) {
    case '+':
        result = num1 + num2;
        break;

    case '-':
        result = num1 - num2;
        break;

    case '*':
        result = num1 * num2;
        break;

    case '/':
        result = num1 / num2;
        break;
}

alert(`Result ${result}`);