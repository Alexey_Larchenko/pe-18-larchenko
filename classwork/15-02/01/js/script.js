document.addEventListener('DOMContent', onReady);

function onReady() {

}

function handleUlClick(event, el) {
    const target = event.target;

    if (target.tagName !== 'LI') {
        return;
    }


    for (let li of el.children) {
        li.classList.remove('active');
    }

    if (target.tagName === 'LI') {
        target.classList.add('active');
    }
}