class Draggable {
    constructor(element) {
        this._element = element;
        this._prepareStyles();
        this.enable();
    }

    enable() {
        this.enabled = true;
        this._onMousedownBound = this._onMousedown.bind(this);
        this._element.addEventListener('mousedown', this._onMousedownBound);
    }

    disable() {
        this.enabled = false;
        this._element.removeEventListener('mousedown', this._onMousedownBound);
    }

    _prepareStyles() {
        this._element.style.position = 'absolute';
    }

    _onMousedown({offsetX, offsetY}) {
        this._offsetX = offsetX;
        this._offsetY = offsetY;

        this._onMousemoveBound = this._onMousemove.bind(this);
        document.addEventListener('mousemove', this._onMousemoveBound);
        this._onMouseupBound = this._onMouseup.bind(this);
        document.addEventListener('mouseup', this._onMouseupBound);
    }

    _onMousemove(event) {
        const {top, left, bottom, right} = this._element.parentElement.getBoundingClientRect();
        let y = Math.max(event.pageY - this._offsetY, top);
        y = Math.min(y, bottom - this._element.offsetHeight);
        let x = Math.max(event.pageX - this._offsetX, left);
        x = Math.min(x, right - this._element.offsetWidth);
        this._element.style.left = `${x}px`;
        this._element.style.top = `${y}px`;
    }

    _onMouseup() {
        document.removeEventListener('mousemove', this._onMousemoveBound);
        document.removeEventListener('mouseup', this._onMouseupBound);
    }
}

const toggleDND = document.querySelector('button');
const dnd = new Draggable(document.querySelector('.box'));
toggleDND.addEventListener('click', () => {
    if (dnd.enabled) {
        dnd.disable();
    }
    else {
        dnd.enable();
    }
});