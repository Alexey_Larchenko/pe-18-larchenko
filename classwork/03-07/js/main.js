class Counter {
    constructor(initialValue) {
        this._createUI();
        this._listenButtons()
        this.updateValue(initialValue);
    }

    _createUI() {
        const container = document.createElement('div');
        const buttonDecrement = document.createElement('button');
        buttonDecrement.innerText = '-';
        const buttonIncrement = document.createElement('button');
        buttonIncrement.innerText = '+';
        const input = document.createElement('input');
        container.append(buttonDecrement);
        container.append(input);
        container.append(buttonIncrement);
        this._input = input;
        this._container = container;
        this._buttonIncrement = buttonIncrement;
        this._buttonDecrement = buttonDecrement;
    }

    _listenButtons() {
        this.decrementBound = this.decrement.bind(this);
        this._buttonDecrement.addEventListener('click', this.decrement.bind(this));
        this.incrementBound = this.increment.bind(this);
        this._buttonIncrement.addEventListener('click', this.increment.bind(this));
    }

    decrement() {
        const newValue = parseFloat(this._input.value) - 1;
        this._input.value = newValue.toString();
        this._onChangeCallback(newValue);
    }

    increment() {
        const newValue = parseFloat(this._input.value) + 1;
        this._input.value = newValue.toString();
        this._onChangeCallback(newValue);
    }

    updateValue(value) {
        this._input.value = value;
    }

    appendTo(parent) {
        parent.append(this._container);
    }

    onChange(callback) {
        this._onChangeCallback = callback;
    }

    destroy() {
        this._container.remove();
        this._buttonDecrement.removeEventListener('click', );
    }
}

const counter = new Counter(7);
counter.appendTo(document.body);
counter.onChange((newVal) => console.log(newVal));