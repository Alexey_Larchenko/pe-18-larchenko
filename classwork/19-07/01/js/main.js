// // class Promise {
// //     _resolves = [];
// //     _rejects = [];
// //     _resolved = false;
// //     _rejected = false;
// //     _val;
// //
// //     constructor(createFn) {
// //         createFn((val) => {
// //             this._resolved = true;
// //             this._resolves.forEach(callback => callback(val));
// //             this._resolves = [];
// //             this._val = val;
// //         }, (val) => {
// //             this._rejected = true;
// //             this._rejects.forEach(callback => callback(val));
// //             this._rejects = [];
// //             this._val = val;
// //         });
// //     }
// //
// //     then(onResolve, onReject) {
// //         if (this._resolved) {
// //             onResolve(this._val);
// //         }
// //         if (this._rejected) {
// //             onReject(this._val);
// //         } else {
// //             this._resolves.push(onResolve);
// //             this._rejects.push(onReject);
// //         }
// //     }
// // }
//
// const promise = new Promise((resolve, reject) => {
//     const button = document.querySelector('button');
//     button.addEventListener('click', event => {
//         resolve(event);
//     }, {once: true});
//
//     const onClick = event => {
//         clearTimeout(timeoutId);
//         console.log('clicked');
//     }
//
//     const timeoutId = setTimeout(() => {
//         console.log('timeout');
//         button.removeEventListener('click', onClick);
//         reject('Not click within 5 secs');
//     }, 5000);
// });
//
// promise.then(result => {
//     console.log(result);
// }, err => {
//     console.log(err);
// });

// Promise.all([
//     getPosts(),
//     getUsers()
// ])
//     .then(([posts, users]) => {
//         posts.forEach(({title, userId}) => {
//             const user = users.find(user => user.id === userId);
//             const p = document.createElement('p');
//             p.innerText = `${user.username}: ${title}`;
//             document.body.append(p);
//         })
//     });

// const container = document.querySelector('#posts');
// let posts, users = [];
// getUsers().then(data => {
//     users = data;
//     if (posts) {
//         printPosts(posts, users);
//     }
// });
//
// getPosts().then(data => {
//     posts = data;
//     printPosts(posts, users);
// });
//
// function printPosts(posts, users) {
//     container.innerHTML = '';
//     posts.forEach(({title, userId}) => {
//         const user = users.find(user => user.id === userId);
//         const p = document.createElement('p');
//         p.innerText = `${user ? user.name : '...'}: ${title}`;
//         document.body.append(p);
//     })
// }
//
// function getUsers() {
//     return wait(Math.random() * 3000)
//         .then(() => fetch('https://jsonplaceholder.typicode.com/users'))
//         .then(res => res.json())
// }
//
// function getPosts() {
//     return wait(Math.random() * 3000)
//         .then(() => fetch('https://jsonplaceholder.typicode.com/posts'))
//         .then(res => res.json())
// }
//
// function wait(ms) {
//     return new Promise(res => setTimeout(res, ms));
// }

// fetch('https://jsonplaceholder.typicode.com/users')
//     .then(response => response.json())
//     .then(users => {
//         return fetch('https://jsonplaceholder.typicode.com/users')
//             .then(response => response.json())
//             .then(posts => [posts, users])
//     })
//     .then(([posts, users]) => {
//         console.log(posts, users);
//     })

// const ctrl = new AbortController();
//
// fetch('https://jsonplaceholder.typicode.com/users', {
//     signal: ctrl.signal
// }).catch(() => {
// })
//
// setTimeout(() => ctrl.abort(), 10);

// async function a() {
//     let response = await fetch('https://jsonplaceholder.typicode.com/users')
//     let data = await response.json();
//     console.log(data);
// }

// document.cookie = 'name=Test';
// document.cookie = 'surname=Surname';

let cookies = document.cookie
    .split(';')
    .map(keyvalue => keyvalue.split('=').map(str => str.trim()));

cookies = Object.fromEntries(cookies);
console.log(cookies.name);
fetch('https://jsonplaceholder.typicode.com/users', {
    credentials: 'include'
});