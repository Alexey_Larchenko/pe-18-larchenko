const express = require('express');

const app = express();

const port = process.env.PORT || 8085;

const emails = [
    { "id": 1, "topic": "Email 1", "body": "Email 1 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." },
    { "id": 2, "topic": "Email 2", "body": "Email 2 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." },
    { "id": 3, "topic": "Email 3", "body": "Email 3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." },
    { "id": 4, "topic": "Email 4", "body": "Email 4 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." },
    { "id": 5, "topic": "Email 5", "body": "Email 5 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." },
    { "id": 6, "topic": "Email 6", "body": "Email 6 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." },
    { "id": 7, "topic": "Email 7", "body": "Email 7 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis vehicula nulla, at cursus nibh condimentum sed. Nulla et vulputate purus, sed sagittis orci. Donec placerat vehicula metus posuere posuere." }
]

app.get('/api/emails', (req, res) => {
    res.send(emails);
})

app.get('/api/emails/:emailId', (req, res) => {
    const { emailId } = req.params;

    const email = emails.find(e => e.id === +emailId);

    if (!email) {
        res.status(404)
            .send('Not found');
    }

    res.send(email);
})

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})