import React, { useState, useEffect } from 'react';
import './App.scss';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Loading from './components/Loading/Loading';
import AppRoutes from './routes/AppRoutes';
import axios from 'axios';
import { connect } from 'react-redux'

const App = () => {
  const [dataLoaded, setDataLoaded] = useState(false);
  const [emails, setEmails] = useState([]);

  useEffect(() => {
    axios('/api/emails')
      .then(res => {
        setEmails(res.data);
        setDataLoaded(true);
      })
  }, [])

  if (!dataLoaded) {
    return <Loading />
  }

  const onDeleteEmail = (id) => {
    setEmails(emails.filter(e => e.id !== id));
  }

  return (
    <div className="App">
      <Header />
      <AppRoutes onDeleteEmail={onDeleteEmail} emails={emails} />
      <Footer />
    </div >
  );
}

const mapStoreToProps = store => {

  return {}
}

export default connect()(App);
