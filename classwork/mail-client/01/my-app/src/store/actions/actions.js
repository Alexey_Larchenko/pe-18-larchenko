// action creator
export const changeTitleAction = (newTitle) => ({
  type: 'CHANGE_TITLE',
  payload: newTitle
})