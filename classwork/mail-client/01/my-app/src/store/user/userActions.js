export const saveUser = (login, password) => ({
  type: 'SAVE_USER',
  payload: {login, password}
})