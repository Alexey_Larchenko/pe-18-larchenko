const initialStore = {
  loading: false,
  data: null,
  // error: null
}

const reducer = (store = initialStore, action) => {
  switch(action.type) {
    case "SAVE_USER":
      return {...store, data: action.payload}
    default:
      return store;
  }
}

export default reducer;