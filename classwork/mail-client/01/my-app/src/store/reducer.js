import { combineReducers } from "redux";
import user from './user/userReducer';
import emails from './email/emailReducer';

const rootReducer = combineReducers({
  user,
  emails
})

export default rootReducer;