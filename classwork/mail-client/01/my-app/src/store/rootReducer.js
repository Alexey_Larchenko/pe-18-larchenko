import { combineReducers } from "redux";
import user from './user/userReducer';
import emails from './email/emailReducer';

const rootReducer = combineReducers({
  // app: combineReducers({
  //   general,
  //   view,
  //   time,
  //   tags,
  //   cuisines
  // }),
  user,
  emails
})

export default rootReducer;