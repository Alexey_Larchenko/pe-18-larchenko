export const getEmailsSelector = (store) => store.emails.data;
export const emailsLoadingSelector = (store) => store.emails.loading;