export const LOADING_EMAILS = 'LOADING_EMAILS';
export const SAVE_EMAILS = 'SAVE_EMAILS';
export const DELETE_EMAIL = 'DELETE_EMAIL'
export const TOGGLE_FAVORITES = 'TOGGLE_FAVORITES'