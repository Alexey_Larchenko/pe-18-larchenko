import React, { useEffect, useState } from 'react'
import Email from '../../components/Email/Email';
import axios from 'axios';
import Loading from '../../components/Loading/Loading';

const OneEmail = (props) => {
  const { match, onDeleteEmail, history } = props;
  const { emailId } = match.params;

  const [email, setEmail] = useState(null);

  useEffect(() => {
    axios(`/api/emails/${emailId}`)
      .then(res => setEmail(res.data))
      .catch(() => history.push('/'));
  }, [emailId, history])

  if (!email) {
    return <Loading />
  }

  return (
    <div>
      <Email showFull email={email} onDeleteEmail={onDeleteEmail} />
    </div>
  )
}

export default OneEmail