import React from 'react';
import './Error.scss';
import deathStar from '../../theme/images/death-star.png';
import { Link } from 'react-router-dom';

const Error = () => {
  return (
    <div className='error'>
      <img className='error__img' src={deathStar} alt='Death star'></img>
      <h2>Something went wrong</h2>
      <h3>But we already sent droids to fix it</h3>
      <Link to='/'>Go to home page</Link>
    </div>
  )


}

export default Error