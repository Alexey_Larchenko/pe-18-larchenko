import React from 'react';
import deathStar from '../../theme/images/death-star.png';
import { Link } from 'react-router-dom';

const Page404 =()=> {
    return (
      <div className='error'>
        <img className='error__img' src={deathStar} alt='Death star'></img>
        <h2>404</h2>
        <h3>Page not found</h3>
        <Link to='/'>Go to home page</Link>
      </div>
    )
}

export default Page404