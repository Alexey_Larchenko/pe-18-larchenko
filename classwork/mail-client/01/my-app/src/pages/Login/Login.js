import React, { useRef } from 'react'
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { saveUser } from '../../store/user/userActions';

const Login = (props) => {
  const { authenticated, loginUser } = props;

  const loginRef = useRef(null);
  const passwordRef = useRef(null);

  if (authenticated) {
    return <Redirect to='/' />
  }

  const submitLoginForm = (e) => {
    e.preventDefault();

    const login = loginRef.current.value
    const password = passwordRef.current.value

    loginUser(login, password);
  }

  return (
    <form onSubmit={submitLoginForm}>
      <div>
        <input type='text' placeholder='Login' ref={loginRef}></input>
      </div>
      <div>
        <input type='password' placeholder='Password' ref={passwordRef}></input>
      </div>
      <div>
        <button type='submit'>Log in</button>
      </div>
    </form>
  )
}

const mapDispatchToProps = (dispatch) => ({
  loginUser: (login, password) => dispatch(saveUser(login, password))
}) 
 
export default connect(null,mapDispatchToProps )(Login)