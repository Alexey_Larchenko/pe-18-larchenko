const routes = {
  login: {
    path: '/login',
    isExact: true
  },
  inbox: {
    path: '/inbox'
  }
}

export default routes;