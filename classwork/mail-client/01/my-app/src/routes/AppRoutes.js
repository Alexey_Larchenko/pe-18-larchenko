import React from 'react';
import Sidebar from '../containers/Sidebar/Sidebar';
import Inbox from '../components/Inbox/Inbox';
import { Route, Switch, Redirect } from 'react-router-dom';
import Sent from '../pages/Sent/Sent';
import Favorites from '../pages/Favorites/Favorites';
import Page404 from '../pages/page404/Page404';
import OneEmail from '../pages/OneEmail/OneEmail';
import Login from '../pages/Login/Login';
import ProtectedRoute from './ProtectedRoute';
import { connect } from 'react-redux';

const AppRoutes = (props) =>{
    const { onDeleteEmail, emails, currentUser } = props;

    const authenticated = !!currentUser;

    return (
      <>
        <ProtectedRoute path='/' component={Sidebar} authenticated={authenticated} />

        <Switch>
          <Route exact path='/' render={() => <Redirect to='/inbox' />} />
          <Route exact path='/login' render={() => <Login authenticated={authenticated}/>} />
          <ProtectedRoute exact path='/sent' component={Sent} authenticated={authenticated} />
          <ProtectedRoute exact path='/favorites' component={Favorites} authenticated={authenticated} />
          <ProtectedRoute exact path='/inbox' render={() => <Inbox onDeleteEmail={onDeleteEmail} emails={emails} />} authenticated={authenticated} />
          <ProtectedRoute
            exact
            path='/inbox/:emailId'
            render={(routerProps) => <OneEmail onDeleteEmail={onDeleteEmail} {...routerProps} />}
            authenticated={authenticated}
          />
          <Route path='*' component={Page404} />
          {/* <Redirect to='/page404' /> */}
        </Switch>
      </>
    )
}

const mapStoreToProps = ({user}) => ({
  currentUser: user.data
})

export default connect(mapStoreToProps)(AppRoutes)