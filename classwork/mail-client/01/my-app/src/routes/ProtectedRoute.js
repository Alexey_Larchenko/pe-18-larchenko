import React from 'react'
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = (props) => {
  const { authenticated, ...rest } = props;

  if (!authenticated) {
    return <Redirect to='/login' />
  }

  return (
    <Route {...rest} />
  )
}

export default ProtectedRoute
