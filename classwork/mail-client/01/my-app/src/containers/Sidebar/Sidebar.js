import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import './Sidebar.scss';

const Sidebar = () => {
    return (
      <div>
        <ul>
          <li><NavLink activeClassName='link-active' exact to='/inbox'>Inbox</NavLink></li>
          <li><NavLink activeClassName='link-active' exact to='/favorites'>Favorites</NavLink></li>
          <li><NavLink activeClassName='link-active' exact to='/sent'>Sent</NavLink></li>
          <li><NavLink activeClassName='link-active' exact to='/login'>Login</NavLink></li>
          <li><Link to='/not-exist'>Not existing page</Link></li>
        </ul>
      </div>
    )
}

export default Sidebar