import { isEmail } from './validation';

test('Validating emails', () => {
  expect(isEmail('aaa@aa.com')).toBeFalsy();
  expect(isEmail('aaa@a.com')).toBeFalsy();
//   expect(isEmail('aaa')).not.toBeFalsy();
//   expect(isEmail('aaa@aaa')).not.toBeFalsy();
//   expect(isEmail('aaa@aa.')).not.toBeFalsy();
//   expect(isEmail('aaa@aa.a')).not.toBeFalsy();
//   expect(isEmail('aaa@aa.aa')).toBeFalsy();
})