import React from 'react';
import './Loading.scss';

const  Loading = ()=> {
    return (
      <h3 className='loading'>Loading...</h3>
    )
}

export default Loading