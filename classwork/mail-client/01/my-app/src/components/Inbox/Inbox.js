import React from 'react';
import Email from "../Email/Email";
import PropTypes from 'prop-types';

const Inbox = (props) => {
    const { onDeleteEmail, emails } = props;

    const emailCards = emails
      .map(email => <Email onDeleteEmail={onDeleteEmail} key={email.id} email={email} />);

    return (
      <div>
        {emailCards}
      </div>
    );
}

Email.propTypes = {
  email: PropTypes.exact({
    id: PropTypes.number,
    topic: PropTypes.string,
    body: PropTypes.string
  }).isRequired,
  onDeleteEmail: PropTypes.func.isRequired,
}

export default Inbox;