import React from 'react';
import './Email.scss';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import Icon from '../Icon/Icon';

const Email = (props) => {
  const { email, onDeleteEmail, className, showFull, history } = props;

  const goBack = () => {
    history.goBack();
  }

  const favorite = email.id === 1;

  return (
    <div className={`email ${className}`}>
      <p>
        <Link to={`/inbox/${email.id}`}>{email.topic}</Link>
        <Icon type='star' filled={favorite} />
        <button onClick={() => onDeleteEmail(email.id)}>X</button>
      </p>

      {showFull && <p>{email.body}</p>}
      {showFull && <button onClick={goBack}>Go back</button>}
    </div>
  )
}

Email.propTypes = {
  email: PropTypes.exact({
    id: PropTypes.number,
    topic: PropTypes.string,
    body: PropTypes.string
  }).isRequired,
  onDeleteEmail: PropTypes.func.isRequired,
  className: PropTypes.string
}

Email.defaultProps = {
  className: ''
}

export default withRouter(Email);