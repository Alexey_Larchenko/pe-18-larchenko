import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { changeTitleAction } from '../../store/actions/actions';

const Header = (props) => {
  const { user, changeTitle } = props;

  return (
    <div className='header'>
      <h3>Mail client</h3>
      {user && <div>{user.login}</div>}
      <button onClick={changeTitle}>Dispatch action</button>
    </div>
  )
}

Header.propTypes = {
  user: PropTypes.shape({
    name: PropTypes.string,
    age: PropTypes.number,
    email: PropTypes.string
  })
}

Header.defaultProps = {
  user: {
    name: 'anonimus2'
  }
}

// mapStateToProps
const mapStoreToProps = ({ user }) => ({
  // title,
  user: user.data
})

const mapDispatchToProps = (dispatch) => {
  return {
    changeTitle: () => dispatch(changeTitleAction('New title in Payload'))
  }
}


export default connect(mapStoreToProps, mapDispatchToProps)(Header)