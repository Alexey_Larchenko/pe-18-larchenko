import React from 'react'

const Footer = () => {
    return (
      <h2>&copy; Dan.IT Education 2020</h2>
    )
}

export default Footer