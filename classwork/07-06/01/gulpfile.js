const {src, dest} = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
}

function buildScss() {
    return src('./src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})
        ]))
        .pipe(dest('./dist'))
}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('.dist/assets'))
}

exports.html = copyHtml();
exports.scss = buildScss();
exports.assets = copyAssets();

exports.default = () => src("style.scss")
    .pipe(sass())
    .pipe(dest("./"));