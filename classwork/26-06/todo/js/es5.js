function Component(props) {
    this.props = props;
    this.container = this.createContainer();
}

// Component.prototype = {
//     createContainer() {
//         return document.createElement('div');
//     }
//     ...
// }

Component.prototype.createContainer = function () {
    return document.createElement('div');
}

Component.prototype.appendTo = function (parent) {
    parent.append(this.container);
}

Component.prototype.destroy = function () {
    this.container.remove();
}


function Application(props) {
    Component.call(this, props);

    const list = new List({list: props.list});
    list.appendTo(this.container);
    const onRemove = (data) => {
        props = {
            ...props,
            list2: props.list2.filter(item => item !== data)
        };

        removableList.destroy();
        removableList = new RemovableList({list: props.list2, onRemove});
        removableList.appendTo(this.container);
    };
    let removableList = new RemovableList({list: props.list2, onRemove});
    removableList.appendTo(this.container);
}

Application.prototype = Object.create(Component.prototype);

Application.prototype.createContainer = function () {
    const el = Component.prototype.createContainer();
    el.classList.add('app');
    return el;

}