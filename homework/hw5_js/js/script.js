function createNewUser(firstName, lastName, birthday) {
    let newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'birthday', {
        value: birthday,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'getLogin', {
        value: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    });
    Object.defineProperty(newUser, 'getPassword', {
        value: function () {
            return firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.slice(6);
        }
    });
    Object.defineProperty(newUser, 'getAge', {
        value: function () {
            return 2020 - birthday.slice(6);
        }
    });

    return newUser;
}

let firstName = prompt('Введите ваше имя:');
let lastName = prompt('Введите вашу фамилию:');
let birthday = prompt('Введите вашу дату рождения:', 'dd.mm.yyyy');

let newUser = createNewUser(firstName, lastName, birthday);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
