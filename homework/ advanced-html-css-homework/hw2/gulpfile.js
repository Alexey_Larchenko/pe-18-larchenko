const {src, dest, series, parallel, watch} = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');
const del = require('del');
const browserSync = require('browser-sync').create();

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyJs() {
    return src('./src/js/**.js')
        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true }));
}

function buildScss() {
    return src('./src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})]))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());
}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'))
}

function emptyDist() {
    return del('./dist/**');
}

const build = series(
    emptyDist,
    parallel(
        series(copyHtml , buildScss),
        copyAssets,
        copyJs
    )
);

function serve() {
    browserSync.init({
        server: './dist'
    });

    watch(['./src/index.html'], copyHtml);
    watch(['./src/scss/*.scss'], buildScss);
    watch(['./src/js/*.js'], copyJs);
}

exports.html = copyHtml;
exports.scss = buildScss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = copyJs;
exports.build = build;
exports.default = series(build, serve);
