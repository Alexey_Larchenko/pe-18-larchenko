import React from 'react';

const Button = (props) => {
    const {description, onClickFunc, className} = props;
    const {backgroundColor, id, text} = description;

    return (
        <button className={className} key={id} onClick={() => onClickFunc()} style={{backgroundColor: backgroundColor }}>{text} </button>
    );
};

export default Button;