const buttonsAndModalsData = {
    firstButton: {
        text: 'Add to cart',
        backgroundColor: '#3cb878',
        id: 'firstButton'
    },
    secondButton: {
        text: 'Remove from cart',
        backgroundColor: '#3cb878',
        id: 'secondButton'
    },
    firstModal: {
        header: 'Attention',
        closeButton: true,
        text: 'Do you want to add this item to cart?',
        actions: [
            {
                id: 'okBtn',
                text: 'Ok',
                backgroundColor: '#3cb878'
            },
            {
                id: 'cancelBtn',
                text: 'Cancel',
                backgroundColor: '#3cb878'
            }
        ]
    },
    secondModal: {
        header: 'Attention',
        closeButton: true,
        text: 'Do you want to remove this item from cart?',
        actions: [
            {
                id: 'okBtn',
                text: 'Ok',
                backgroundColor: '#3cb878'
            },
            {
                id: 'cancelBtn',
                text: 'Cancel',
                backgroundColor: '#3cb878'
            }
        ]
    },
}

export default buttonsAndModalsData;