export const formDataAction = (value) => dispatch => dispatch({
    type: 'SET_FORM_DATA', payload: value
})