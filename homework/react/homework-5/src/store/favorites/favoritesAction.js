export const setToFavoritesAction = (products) => dispatch => dispatch({
    type: 'SET_TO_FAVORITES',
    payload: products
});