export const showFirstModalAction = () => dispatch => dispatch({type: 'SHOW_FIRST_MODAL'});

export const showSecondModalAction = () => dispatch => dispatch({type: 'SHOW_SECOND_MODAL'});