import axios from 'axios';

export const loadDataAction = () => dispatch => {
    axios('/shoppingGoods.json')
        .then(res => {
            dispatch({type: 'SAVE_PRODUCTS_DATA', payload: res.data});
        })
};