const initialStore = {
  goodsData: [],
  showFirstModal: false,
  showSecondModal: false,
  addToBasket: null,
  removeFromBasket: null,
  favorites: [],
  basket: [],
  formData: {
    firstName: '',
    lastName: '',
    age: '',
    address: '',
    phone: ''
  }
};

const reducer = (store = initialStore, action) => {
  switch (action.type) {
    case 'SAVE_PRODUCTS_DATA':
      return {...store, goodsData: action.payload};
    case 'SHOW_FIRST_MODAL':
      return {...store, showFirstModal: !store.showFirstModal};
    case 'SHOW_SECOND_MODAL':
      return {...store, showSecondModal: !store.showSecondModal};
    case 'ADD_PRODUCTS_TO_BASKET':
      return {...store, addToBasket: action.payload};
    case 'REMOVE_PRODUCTS_FROM_BASKET':
      return {...store, removeFromBasket: action.payload};
    case 'SET_PRODUCTS_TO_BASKET':
      return {...store, basket: action.payload};
    case 'SET_TO_FAVORITES':
      return {...store, favorites: action.payload};
    case 'SET_FORM_DATA':
      return {...store, formData: action.payload};
    case 'CLEAR_BASKET':
      return {...store, basket: []}
    default:
      return store
  }
};

export default reducer;