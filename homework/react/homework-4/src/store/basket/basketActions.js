export const setToBasketAction = (products) => dispatch => dispatch({
    type: 'SET_PRODUCTS_TO_BASKET',
    payload: products
});

export const addToBasketAction = (id) => dispatch => dispatch({
    type: 'ADD_PRODUCTS_TO_BASKET',
    payload: id
});

export const removeFromBasketAction = (id) => dispatch => dispatch({
    type: 'REMOVE_PRODUCTS_FROM_BASKET',
    payload: id
});