import React from 'react';
import Card from "../../components/Card/Card";
import buttonsAndModalsData from "../buttonsAndModalsData";
import Button from "../../components/Button/Button";
import Modal from "../Modal/Modal";
import './Body.scss';
import {useDispatch, useSelector} from "react-redux";
import { addToBasketSelector, basketSelector, productsDataSelector, showFirstModalSelector,
    removeFromBasketSelector, showSecondModalSelector } from "../../store/selectors/selectors";
import {setToFavoritesAction} from "../../store/favorites/favoritesAction";
import {addToBasketAction, removeFromBasketAction, setToBasketAction} from "../../store/basket/basketActions";
import {showFirstModalAction, showSecondModalAction} from "../../store/modals/modalsActions";

const Body = (props) => {
    const {cardData} = props;
    const dispatch = useDispatch();
    const {firstButton, firstModal, secondModal} = buttonsAndModalsData;
    
    const basket = useSelector(basketSelector);
    const productsData = useSelector(productsDataSelector);
    const firstModalOpen = useSelector(showFirstModalSelector);
    const secondModalOpen = useSelector(showSecondModalSelector);
    const addingInBasketCode = useSelector(addToBasketSelector);
    const removingFromBasketCode = useSelector(removeFromBasketSelector);

    const openFirstModal = () => dispatch(showFirstModalAction());
    const openSecondModal = () => dispatch(showSecondModalAction());
    const addToBasket = (id) => dispatch(addToBasketAction(id));
    const removeFromBasket = (id) => dispatch(removeFromBasketAction(id));
    const setToBasket = products => dispatch(setToBasketAction(products));
    const setToFavorites = products => dispatch(setToFavoritesAction(products));

    const showFirstModal = (id) => {
        {id &&  addToBasket(id)}
        openFirstModal();
        removeFromBasket(null);
    };

    const showSecondModal = (id) => {
        {id &&  removeFromBasket(id)}
        openSecondModal();
        addToBasket('');
    };
    
    const addItemToLocalStorage = (id, value) => {
        const values = JSON.parse(localStorage.getItem(value));

        if(values.findIndex(elem => elem === id) !== -1 && values.length !== 0) {
            const index = values.findIndex(elem => elem === id);
            values.splice(index, 1);
            localStorage.setItem(value, JSON.stringify(values));
        } else {
            values.push(id);
            localStorage.setItem(value, JSON.stringify(values));
        }

        value === 'basket' && setToBasket(values);
        value === 'favoriteProducts' && setToFavorites(values);
    };

    const Cards = () => {
        return productsData.map(data => {
            if(cardData.find(elem => elem === data.id)) {

                let inBasket;
                basket.find(elem => elem === data.id) ? inBasket = false: inBasket = true;

                return <Card firstButton={firstButton}
                                    addItemToLocalStorage={addItemToLocalStorage}
                                    showFirstModal={showFirstModal}
                                    showSecondModal={showSecondModal}
                                    data={data}
                                    key={data.id}
                                    inBasket={inBasket}
                />
            }
        });
    };

    const firstModalButtons = firstModal.actions.map(button => {
        switch (button.id) {
            case "okBtn":
                return <Button className='main-btn' onClickFunc={() => {
                    addItemToLocalStorage(addingInBasketCode, 'basket');
                    openFirstModal();
                }}
                               key={button.id}
                               description={button}/>;
            case "cancelBtn":
                return <Button className='main-btn' onClickFunc={() => showFirstModal()} key={button.id} description={button}/>
        }
    });

    const secondModalButtons = secondModal.actions.map(button => {
        switch (button.id) {
            case "okBtn":
                return <Button className='main-btn' onClickFunc={() => {
                    addItemToLocalStorage(removingFromBasketCode, 'basket');
                    showSecondModal();
                }}
                               key={button.id}
                               description={button}/>;
            case "cancelBtn":
                return <Button className='main-btn' onClickFunc={() => showSecondModal()} key={button.id} description={button}/>
        }
    });

    return (
        <div className='cards-container'>
            {Cards()}
            {firstModalOpen && <Modal modal={firstModal}
                                      onClickFunc={showFirstModal}
                                      additionalButtons={firstModalButtons}
                                      header={firstModal.header}
                                      text={firstModal.text}
            />}
            {secondModalOpen && <Modal modal={secondModal}
                                       onClickFunc={showSecondModal}
                                       additionalButtons={secondModalButtons}
                                       header={secondModal.header}
                                       text={secondModal.text}
            />}
        </div>
    );

};

export default Body;