import React from 'react';
import './Modal.scss';
import '../../components/Button/Button.scss';

function Modal(props) {
    const {header, text, modal, onClickFunc, additionalButtons} = props
    const {closeButton} = modal;

    return (
        <>
            <div className='modal-window' onClick={() => onClickFunc()}/>
            <div className='modal-content'>
                <div className="modal-header">
                    <h3 className='modal-title'>{header}</h3>
                    {closeButton && <span className='close' onClick={() => onClickFunc()}/>}
                </div>
                <div className="modal-body">
                    <p className='modal-text'>{text}</p>
                </div>
                <div className="modal-footer">
                    {additionalButtons}
                </div>
            </div>
        </>
    );
}

export default Modal;