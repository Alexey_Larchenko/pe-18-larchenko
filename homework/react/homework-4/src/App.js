import React, {useEffect} from 'react';
import './App.css';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {useDispatch} from "react-redux";
import {loadDataAction} from "./store/products/productsActions";
import {setToBasketAction} from "./store/basket/basketActions";
import {setToFavoritesAction} from "./store/favorites/favoritesAction";

const App = () => {
    const dispatch = useDispatch();
    const setToBasket = products => dispatch(setToBasketAction(products));
    const setToFavorites = products => dispatch(setToFavoritesAction(products));

    useEffect(() => {
        dispatch(loadDataAction())
    }, [dispatch]);

    useEffect(() => {
        const storageBasket = setLocalStorage('basket');
        const storageFavorites = setLocalStorage('favoriteProducts');

        setToBasket(storageBasket);
        setToFavorites(storageFavorites);
    }, []);

    const setLocalStorageArray = (name) => localStorage.setItem(name, JSON.stringify([]));
    const setLocalStorage = (el) => {
        let item = JSON.parse(localStorage.getItem(el));

        if (item === null) {
            item = [];
            setLocalStorageArray(el);
        }

        return item;
    }

    return (
        <>
            <Header/>
            <AppRoutes/>
        </>
    )
};

export default App;