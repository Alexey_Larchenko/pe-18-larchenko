import React, {useState, useEffect} from 'react';
import Header from "./components/Header/Header";
import axios from 'axios';
import AppRoutes from "./routes/AppRoutes";

function App() {

    const [addToBasket, setAddToBasket] = useState(null);
    const [basket, setBasket] = useState([]);
    const [cardId, setCardId] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [goodsData, setGoodsData] = useState([]);
    const [openFirstModal, setOpenFirstModal] = useState(false);
    const [openSecondModal, setOpenSecondModal] = useState(false);
    const [removeFromBasket, setRemoveFromBasket] = useState(null);

    useEffect(() => {
        axios('/shoppingGoods.json')
            .then(res => {
                setGoodsData(res.data);

                const arrayOfCardId = [];
                res.data.forEach(el => arrayOfCardId.push(el.id));
                setCardId(arrayOfCardId);
            })
    }, [])

    useEffect(() => {
        const basketData = JSON.parse(localStorage.getItem('basket'));
        const favoritesData = JSON.parse(localStorage.getItem('favoriteProducts'));

        setBasket(basketData);
        setFavorites(favoritesData);

        if (basketData === null) {
            setItemToLocalStorage('basket');
        }

        if (favoritesData === null) {
            setItemToLocalStorage('favoriteProducts');
        }

    }, [])


    const showFirstModal = (id) => {
        id && setAddToBasket(id)
        setOpenFirstModal(!openFirstModal);
        setRemoveFromBasket(null);
        setOpenSecondModal(false);
    };

    const showSecondModal = (id) => {
        id && setRemoveFromBasket(id)
        setOpenSecondModal(!openSecondModal);
        setAddToBasket(null);
        setOpenFirstModal(false);
    };


    const addItemToLocalStorage = (id, value, modal = true) => {
        const data = JSON.parse(localStorage.getItem(value));

        if (data.findIndex(elem => elem === id) !== -1 && data.length !== 0) {
            const index = data.findIndex(elem => elem === id);
            data.splice(index, 1);
            localStorage.setItem(value, JSON.stringify(data));
        } else {
            data.push(id);
            localStorage.setItem(value, JSON.stringify(data));
        }

        (value === 'basket' && modal && setOpenFirstModal(!openFirstModal));

        value === 'basket' && setBasket(data);
        value === 'favoriteProducts' && setFavorites(data);
    };

    const setItemToLocalStorage = (el) => {
        localStorage.setItem(el, JSON.stringify([]));
        return [];
    };

    return <>
        <Header favorites={favorites} basket={basket}/>
        <AppRoutes addToBasket={addToBasket}
                   addItemToLocalStorage={addItemToLocalStorage}
                   basket={basket}
                   favorites={favorites}
                   goodsData={goodsData}
                   openFirstModal={openFirstModal}
                   openSecondModal={openSecondModal}
                   removeFromBasket={removeFromBasket}
                   showFirstModal={showFirstModal}
                   showSecondModal={showSecondModal}
                   cardId={cardId}
        />
    </>
}

export default App;
