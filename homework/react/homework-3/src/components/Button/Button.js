import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
    const {description, onClickFunc, className} = props;
    const {backgroundColor, id, text} = description;

    return (
        <button className={className} key={id} onClick={() => onClickFunc()} style={{backgroundColor: backgroundColor }}>{text} </button>
    );
};

Button.propTypes = {
    description: PropTypes.object,
    onClickFunc: PropTypes.func,
    backgroundColor: PropTypes.string,
    id: PropTypes.string,
    text: PropTypes.string
};

export default Button;
