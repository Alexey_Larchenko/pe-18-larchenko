import React from 'react';
import Card from "../../components/Card/Card";
import buttonsAndModalsData from "../buttonsAndModalsData";
import Button from "../../components/Button/Button";
import Modal from "../Modal/Modal";
import './Body.scss';

const Body = (props) => {
    const {cardId} = props;
    const {basket, favorites, addItemToLocalStorage, showFirstModal, openFirstModal, openSecondModal, goodsData, addToBasket, removeFromBasket, showSecondModal} = props.data;
    const {firstButton, firstModal, secondModal} = buttonsAndModalsData;

    const firstModalButtons = firstModal.actions.map(button => {
        if (button.id === 'okBtn') {
            return <Button className='action-btn' onClickFunc={() => {
                addItemToLocalStorage(addToBasket, 'basket');
            }}
                           key={button.id}
                           description={button}/>;
        }

        if (button.id === 'cancelBtn') {
            return <Button className='action-btn' onClickFunc={() => showSecondModal()} key={button.id}
                           description={button}/>
        }
    });

    const secondModalButtons = secondModal.actions.map(button => {
        if (button.id === 'okBtn') {
            return <Button className='action-btn' onClickFunc={() => {
                addItemToLocalStorage(removeFromBasket, 'basket');
                showSecondModal();
            }}
                           key={button.id}
                           description={button}/>;
        }

        if (button.id === 'cancelBtn') {
            return <Button className='action-btn' onClickFunc={() => showSecondModal()} key={button.id}
                           description={button}/>
        }
    });

    const Cards = () => {
        return goodsData.map(data => {
            if (cardId.find(item => item === data.id)) {

                let productInBasket;
                basket.find(item => item === data.id) ? productInBasket = false : productInBasket = true;

                return <Card firstButton={firstButton}
                             addItemToLocalStorage={addItemToLocalStorage}
                             showFirstModal={showFirstModal}
                             showSecondModal={showSecondModal}
                             favorites={favorites}
                             basket={basket}
                             data={data}
                             key={data.id}
                             productInBasket={productInBasket}
                />
            }
        });
    };

    return (
        <>
            {openFirstModal && <Modal modal={firstModal}
                                      onClickFunc={showFirstModal}
                                      additionalButtons={firstModalButtons}
                                      header={firstModal.header}
                                      text={firstModal.text}
            />}
            {openSecondModal && <Modal modal={secondModal}
                                       onClickFunc={showSecondModal}
                                       additionalButtons={secondModalButtons}
                                       header={secondModal.header}
                                       text={secondModal.text}
            />}
            <div className='cards-container'>
                {Cards()}
            </div>
        </>
    );

};

export default Body;
