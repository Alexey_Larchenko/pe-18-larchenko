import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Body from "../components/Body/Body";

function AppRoutes(props) {
    const {basket, favorites, cardId} = props;

    return (
        <>
            <Switch>
                <Route exect path='/basket' render={() => <Body data={props} cardId={basket}/>}/>
                <Route exect path='/favorites' render={() => <Body data={props} cardId={favorites}/>}/>
                <Route exect path='/products' render={() => <Body data={props} cardId={cardId}/>}/>
            </Switch>
        </>
    );
}

export default AppRoutes;