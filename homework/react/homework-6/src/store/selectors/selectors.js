export const basketSelector = (store) => store.basket;
export const productsDataSelector = (store) => store.goodsData;
export const showFirstModalSelector = (store) => store.showFirstModal;
export const showSecondModalSelector = (store) => store.showSecondModal;
export const favoriteProductsSelector = (store) => store.favorites;
export const addToBasketSelector = (store) => store.addToBasket;
export const removeFromBasketSelector = (store) => store.removeFromBasket;
export const getFormDataSelector = (store) => store.formData;