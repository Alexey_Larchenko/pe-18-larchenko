import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Body from "../components/Body/Body";
import {useSelector} from "react-redux";
import {basketSelector, productsDataSelector, favoriteProductsSelector} from "../store/selectors/selectors";
import Basket from "../components/Basket/Basket";

const AppRoutes = () => {
    const productsData = useSelector(productsDataSelector);
    const basket = useSelector(basketSelector);
    const favoriteProducts = useSelector(favoriteProductsSelector);

    const cardId = [];
    productsData.map( card => cardId.push(card.id));

    return (
        <>
            <Switch>
                <Route exect path='/basket' render={()=> <Basket/>}/>
                <Route exect path='/favorites' render={()=> <Body cardData={favoriteProducts}/>}/>
                <Route path='/' render={()=> <Body cardData={cardId}/>}/>
            </Switch>
        </>
    );
};

export default AppRoutes;