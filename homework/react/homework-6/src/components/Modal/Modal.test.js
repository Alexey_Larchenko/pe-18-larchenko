import React from "react";
import Modal from "./Modal";
import {shallow, configure} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';

const modal = {
    header: 'Attention',
    closeButton: true,
    text: 'Do you want to add this item to cart?',
    actions: [
        {
            id: 'okBtn',
            text: 'Ok',
            backgroundColor: '#3cb878'
        },
        {
            id: 'cancelBtn',
            text: 'Cancel',
            backgroundColor: '#3cb878'
        }
    ]
}

configure({adapter: new Adapter()});

test('Modal is rendered', () => {
    shallow(<Modal modal={modal} header={modal.header} text={modal.text} additionalButtons={modal.actions}/>)
});


test('Close button exist', () => {
    const wrapper = shallow(<Modal modal={modal} header={modal.header} text={modal.text} additionalButtons={modal.actions}/>);

    expect(wrapper.find('.close').exists()).not.toBeFalsy();
});

test('Modal may close', () => {
    const mockFunction = jest.fn();
    const wrapper = shallow(<Modal onClickFunc={mockFunction} modal={modal} header={modal.header} text={modal.text} additionalButtons={modal.actions}/>);

    wrapper.find('.close').simulate('click');

    expect(mockFunction).toHaveBeenCalled();
});
