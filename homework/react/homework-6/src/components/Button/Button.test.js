import React from "react";
import Button from "./Button";
import {shallow, configure} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';


const description = {
    button: {
        id: 3,
        text: 'Test button',
        backgroundColor: '#f0f0f0'
    }
};

configure({adapter: new Adapter()});

test('Btn is rendered', () => {
    shallow(<Button description={description}/>)
});

test('mockFunction called', () => {
    const mockFunction = jest.fn();
    const testBtn = shallow(<Button onClickFunc={mockFunction} description={description}/>);

    testBtn.find('button').simulate('click');

    expect(mockFunction).toHaveBeenCalled();
});
