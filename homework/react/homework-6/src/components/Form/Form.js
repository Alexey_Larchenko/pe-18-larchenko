import React from 'react'
import {Form, Field, Formik, ErrorMessage} from 'formik';
import * as yup from 'yup';
import {clearBasketAction} from "../../store/basket/basketActions";
import {useDispatch, useSelector} from "react-redux";
import {basketSelector, productsDataSelector, getFormDataSelector} from "../../store/selectors/selectors";
import {formDataAction} from "../../store/form/formAction";
import './Form.scss';

const formSchema = yup.object().shape({
    firstName: yup
        .string()
        .required("This field is required")
        .min(3, "Minimum 3 symbols"),
    lastName: yup
        .string()
        .required("This field is required")
        .min(3, "Minimum 3 symbols"),
    age: yup
        .number()
        .required("This field is required")
        .positive()
        .integer(),
    address: yup
        .string()
        .required("This field is required")
        .min(5, "Minimum 5 symbols"),
    phone: yup
        .number()
        .integer()
        .positive()
        .required("This field is required")
});

const setLocalStorageArray = (firstName) => localStorage.setItem(firstName, JSON.stringify([]));
const setLocalStorage = (el) => {
    let item = JSON.parse(localStorage.getItem(el));

    if (item === null) {
        item = [];
        setLocalStorageArray(el);
    }

    return item;
}

function CheckoutForm() {
    const dispatch = useDispatch();
    const basket = useSelector(basketSelector);
    const productsData = useSelector(productsDataSelector);
    const formData = useSelector(getFormDataSelector);

    const clearBasket = () => dispatch(clearBasketAction());
    const setFormData = (values) => dispatch(formDataAction(values));

    const handleSubmit = (values, {setSubmitting}) => {
        setSubmitting(false);
        console.log('Form data:', values);

        const purchasedGoods = [];
        productsData.map( card => {
            basket.forEach( id => {
                if(card.id === id){
                    purchasedGoods.push(card);
                }
            })
        });
        console.log('Purchased good:', purchasedGoods);
        clearBasket();
        setLocalStorage('basket');
        setFormData({
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phone: ''
        });
    };

    return (
        <Formik
            initialValues = {{
                firstName: formData.firstName,
                lastName: formData.lastName,
                age: formData.age,
                address: formData.address,
                phone: formData.phone
            }}
            validationSchema={formSchema}
            onSubmit={handleSubmit}
        >

            {(formikProps) => {
                setFormData(formikProps.values);
                return (
                    <Form onSubmit={formikProps.handleSubmit} noValidate className='form'>
                        <h3>Checkout</h3>
                        <Field component='input' type='text' name='firstName' placeholder='First name'/>
                        <span className='error'><ErrorMessage name='firstName' /></span>
                        <Field component='input' type='text' name='lastName' placeholder='Last name'/>
                        <span className='error'><ErrorMessage name='lastName' /></span>
                        <Field component='input' type='number' name='age' placeholder='Age'/>
                        <span className='error'><ErrorMessage name='age' /></span>
                        <Field component='input' type='text' name='address' placeholder='Address'/>
                        <span className='error'><ErrorMessage name='address' /></span>
                        <Field component='input' type='text' name='phone' placeholder='Phone'/>
                        <span className='error'><ErrorMessage name='phone' /></span>
                        <div>
                            <button className='form-btn' type='submit' disabled={formikProps.isSubmitting}>Checkout</button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
}

export default CheckoutForm;