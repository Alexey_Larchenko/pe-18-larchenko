import React from 'react';
import {useSelector} from "react-redux";
import {basketSelector} from "../../store/selectors/selectors";
import Body from "../Body/Body";
import CheckoutForm from "../Form/Form";

function Basket() {
    const basket = useSelector(basketSelector);
    return (
        <>
            {basket.length > 0 && <CheckoutForm/>}
            <Body cardData={basket}/>
        </>
    );
}

export default Basket;