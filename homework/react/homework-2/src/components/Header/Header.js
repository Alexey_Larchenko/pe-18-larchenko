import React, {Component} from 'react';
import './Header.scss';

class Header extends Component {
    render() {
        const {setItemToLocalStorage} = this.props;

        const goodsAmount = (favoriteAmount, basketAmount) => {
            const inFavorite = JSON.parse(localStorage.getItem('favoriteProducts'));
            const inBasket = JSON.parse(localStorage.getItem('basket'));

            inFavorite === null ? favoriteAmount = setItemToLocalStorage('favoriteProducts') : favoriteAmount = inFavorite.length;
            inBasket === null ? basketAmount = setItemToLocalStorage('basket') : basketAmount = inBasket.length;

            return <div className='header-stat'>
                <span>In the favorite there are: <span style={{fontWeight: 600, fontSize: 19}}>{favoriteAmount}</span> items</span>
                <span>In the basket there are: <span style={{fontWeight: 600, fontSize: 19}}>{basketAmount}</span> items</span><br/>
            </div>
        };

        return (
            <header className='header'>
                {goodsAmount()}
            </header>
        );
    }
}

export default Header;