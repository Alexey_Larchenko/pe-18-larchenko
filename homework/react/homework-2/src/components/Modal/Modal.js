import React, {Component} from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';
import '../../components/Button/Button.scss';

class Modal extends Component {
    render() {
        const {header, text, modal, onClickFunc, name, additionalButtons} = this.props
        const {closeButton} = modal;

        return (
            <>
                <div className='modal-window' onClick={() => onClickFunc(name, false)}/>
                <div className='modal-content'>
                    <div className="modal-header">
                        <h3 className='modal-title'>{header}</h3>
                        {closeButton && <span className='close' onClick={() => onClickFunc(name, false)}/>}
                    </div>
                    <div className="modal-body">
                        <p className='modal-text'>{text}</p>
                    </div>
                    <div className="modal-footer">
                        {additionalButtons}
                    </div>
                </div>
            </>
        );
    }
}

export default Modal;
