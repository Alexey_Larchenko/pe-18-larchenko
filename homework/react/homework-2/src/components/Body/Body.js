import React, {Component} from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import buttonsAndModalsData from "../buttonsAndModalsData";
import Card from "../Card/Card";
import './Body.scss'

class Body extends Component {
    render() {
        const {addItemToLocalStorage, showFirstModal, data} = this.props;
        const {showModal, addToBasket, goodsData} = data;
        const {firstButton, firstModal} = buttonsAndModalsData;
        const Cards = goodsData.map(data => {
            return <Card firstButton={firstButton}
                         addItemToLocalStorage={addItemToLocalStorage}
                         showFirstModal={showFirstModal}
                         data={data}
                         key={data.id}
            />
        });

        const modalAdditionalButtons = firstModal.actions.map(button => {
            if (button.id === 'okBtn') {
                return <Button className='action-btn' onClickFunc={() => addItemToLocalStorage(addToBasket, 'basket')}
                               key={button.id}
                               description={button}/>
            }
            if (button.id === 'cancelBtn') {
                return <Button className='action-btn' onClickFunc={() => showFirstModal()} key={button.id}
                               description={button}/>
            }
        });

        return (
            <>
                {showModal && <Modal modal={firstModal}
                                     header={firstModal.header}
                                     text={firstModal.text}
                                     onClickFunc={showFirstModal}
                                     additionalButtons={modalAdditionalButtons}
                />}
                <div className='cards-container'>
                    {Cards}
                </div>
            </>
        );
    }
}

export default Body;
