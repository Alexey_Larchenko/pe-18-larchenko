import React, {Component} from 'react';

class Button extends Component {
    render() {
        const {description, onClickFunc, className} = this.props;
        const {backgroundColor, id, text} = description;

        return (
            <button className={className} key={id} onClick={() => onClickFunc()}
                    style={{backgroundColor: backgroundColor}}>{text} </button>
        );
    }
}

export default Button;
