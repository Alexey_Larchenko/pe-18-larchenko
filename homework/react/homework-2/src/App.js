import React, {Component} from 'react';
import Body from "./components/Body/Body";
import Header from "./components/Header/Header";
import axios from 'axios';

class App extends Component {
    state = {
        showModal: false,
        addToBasket: null,
        goodsData: [],
        favorites: [],
        basket: []
    };

    componentDidMount() {
        axios('/shoppingGoods.json')
            .then(res => this.setState(() => {
                return {goodsData: [...res.data]}
            }))
    }

    showFirstModal = (id) => {
        {
            id && this.setState({addToBasket: id})
        }
        this.setState({showModal: !this.state.showModal});
    };

    addItemToLocalStorage = (id, value, modal = true) => {
        const data = JSON.parse(localStorage.getItem(value));

        if (data.findIndex(elem => elem === id) !== -1 && data.length !== 0) {
            const index = data.findIndex(elem => elem === id);
            data.splice(index, 1);
            localStorage.setItem(value, JSON.stringify(data));
        } else {
            data.push(id);
            localStorage.setItem(value, JSON.stringify(data));
        }

        (value === 'basket' && modal && this.setState(() => {
                return {
                    showModal: !this.state.showModal
                }
            })
        );

        this.setState(() => {
            return {[value]: data}
        })
    };

    setItemToLocalStorage = (el) => {
        localStorage.setItem(el, JSON.stringify([]));
        return [];
    };

    render() {
        return <>
            <Header setItemToLocalStorage={this.setItemToLocalStorage}/>
            <Body data={this.state} showFirstModal={this.showFirstModal}
                  addItemToLocalStorage={this.addItemToLocalStorage}/>
        </>
    }
}

export default App;
