const buttonsAndModalsData = {
    firstButton: {
        text: 'Open first modal',
        backgroundColor: '#3cb878'
    },
    secondButton: {
        text: 'Open second modal',
        backgroundColor: '#B3382C'
    },
    firstModal: {
        header: 'Header',
        display: 'block',
        closeButton: true,
        text: 'Main modal text',
        actions: [
            {
                id: 'okBtn',
                text: 'Ok',
                backgroundColor: '#3cb878'
            },
            {
                id: 'cancelBtn',
                text: 'Cancel',
                backgroundColor: '#3cb878'
            }
        ]
    },
    secondModal: {
        header: 'Do you want to delete this file?',
        closeButton: true,
        text: 'Once you delete this file, it won’t be possible to undo this action. \n' +
            'Are you sure you want to delete it?',
        actions: [
            {
                id: 'okBtn',
                text: 'Ok',
                backgroundColor: '#B3382C'
            },
            {
                id: 'cancelBtn',
                text: 'Cancel',
                backgroundColor: '#B3382C'
            }
        ]
    }
}

export default buttonsAndModalsData;