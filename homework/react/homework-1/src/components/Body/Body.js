import React, {Component} from 'react';
import buttonsAndModalsData from "../buttonsAndModalsData";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import './Body.scss';

class Body extends Component {
    render() {
        const {openModal, value} = this.props;
        const {showFirstModal, showSecondModal} = value;
        const {firstButton, secondButton, firstModal, secondModal} = buttonsAndModalsData;

        return (
            <div className='container'>
                {showFirstModal &&
                <Modal
                    name={'showFirstModal'}
                    header={firstModal.header}
                    text={firstModal.text}
                    modal={firstModal}
                    openModal={openModal}
                />}
                {showSecondModal &&
                <Modal
                    name={'showSecondModal'}
                    header={secondModal.header}
                    text={secondModal.text}
                    modal={secondModal}
                    openModal={openModal}
                />}

                <Button
                    openModal={() => openModal('showFirstModal', true)}
                    text={firstButton.text}
                    backgroundColor={firstButton.backgroundColor}
                />
                <Button
                    openModal={() => openModal('showSecondModal', true)}
                    text={secondButton.text}
                    backgroundColor={secondButton.backgroundColor}
                />
            </div>
        );
    }
}

export default Body;