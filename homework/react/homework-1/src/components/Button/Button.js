import React, {Component} from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {

    render() {
        const {text, openModal, backgroundColor} = this.props

        return (
            <div>
                <button className='main-btn' onClick={openModal} style={{backgroundColor: backgroundColor}}>{text}</button>
            </div>
        );
    }
}

Button.propTypes = {
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    openModal: PropTypes.func
}

export default Button;