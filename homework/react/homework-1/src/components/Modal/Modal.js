import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';

class Modal extends Component {
    render() {
        const {header, text, modal, openModal, name} = this.props
        const {closeButton, actions} = modal;

        const modalButtons = actions.map(btn => {
            return <button className='action-btn' style={{backgroundColor: btn.backgroundColor}} key={btn.id}>{btn.text}</button>
        });

        return (
            <>
                <div className='modal-window' onClick={() => openModal(name, false)}/>
                <div className='modal-content'>
                    <div className="modal-header">
                        <h3 className='modal-title'>{header}</h3>
                        {closeButton && <span className='close' onClick={() => openModal(name, false)}/>}
                    </div>
                    <div className="modal-body">
                        <p className='modal-text'>{text}</p>
                    </div>
                    <div className="modal-footer">
                        {modalButtons}
                    </div>
                </div>
            </>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    name: PropTypes.string,
    openModal: PropTypes.func,
    actions: PropTypes.array,
    modal: PropTypes.object,
    closeButton: PropTypes.bool
}

export default Modal;