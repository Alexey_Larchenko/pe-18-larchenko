import React, {Component} from 'react';
import './App.scss';
import Body from "./components/Body/Body";

class App extends Component {
    state = {
        showFirstModal: false,
        showSecondModal: false
    }

    render() {
        return (
            <Body value={this.state} openModal={this.openModal}/>
        );
    }

    openModal = (name, isOpen) => {
        this.setState({[name]: isOpen});
    }
}

export default App;