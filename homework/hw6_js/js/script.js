function filterBy(array, number) {
    return array.filter(item => typeof item !== number);
}
let array = [1, null, 15, undefined, [1,'work'], {name: 'Jack'}, 'hello', 105];
console.log(filterBy(array, 'number'));