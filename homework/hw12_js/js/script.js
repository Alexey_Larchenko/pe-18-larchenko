function swapImg() {
    let i = 0;
    let images = ['./img/1.jpg', './img/2.jpg', './img/3.JPG', './img/4.png'];
    let element = document.getElementById('image-to-show');

    function toggle() {
        element.src = images[i];
        i = (i + 1) % images.length;
    }

    let timer = setInterval(toggle, 1000);

    let stopBtn = document.getElementById('stopBtn');
    stopBtn.addEventListener('click', function () {
        clearInterval(timer);
    });
}