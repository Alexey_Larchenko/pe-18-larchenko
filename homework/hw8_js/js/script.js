function inputFocus() {
    let inputBorder = document.getElementById('price-input');
    inputBorder.style.border = '3px solid lime';
    errorSpan.remove();
}

let button = document.getElementById('button-x');
let spanElement = document.createElement('span');
let errorSpan = document.createElement('span');

function inputWithoutFocus() {
    let inputBorder = document.getElementById('price-input');
    inputBorder.style.border = '1px solid grey';
    spanElement.innerHTML = `Current price: $${document.getElementById('price-input').value}`;
    document.body.before(spanElement);

    button.style.visibility = 'visible';
    document.body.before(button);
    document.getElementById('price-input').style.backgroundColor = '#3cb878';

    if (document.getElementById('price-input').value <= 0) {
        inputBorder.style.border = '3px solid red';
        document.getElementById('price-input').style.backgroundColor = '#fff';
        document.getElementById('price-input').value = null;
        spanElement.remove();
        button.remove();

        errorSpan.innerHTML = 'Please enter correct price';
        document.body.after(errorSpan);
    }
}
button.onclick = deletePrice;

function deletePrice() {
    spanElement.remove();
    button.remove();
    document.getElementById('price-input').style.backgroundColor = '#fff';
    document.getElementById('price-input').value = null;
}