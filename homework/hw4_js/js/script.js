let firstName = prompt('Введите ваше имя:');
let lastName = prompt('Введите вашу фамилию:');

function createNewUser(firstName, lastName) {
    let newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'getLogin', {
        value: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    });

    return newUser;
}

let newUser = createNewUser(firstName, lastName);
console.log(newUser.getLogin());