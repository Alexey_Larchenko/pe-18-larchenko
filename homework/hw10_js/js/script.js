function showFirstPassword(target) {
    let password = document.getElementById("firstPassword");
    if (password.getAttribute("type") === "password") {
        target.classList.add('fa-eye-slash');
        password.setAttribute("type", "text");
    } else {
        target.classList.remove('fa-eye-slash');
        password.setAttribute("type", "password");
    }
}

function showSecondPassword(target) {
    let password = document.getElementById("secondPassword");
    if (password.getAttribute("type") === "password") {
        target.classList.add('fa-eye-slash');
        password.setAttribute("type", "text");
    } else {
        target.classList.remove('fa-eye-slash');
        password.setAttribute("type", "password");
    }
}

function checkPass() {
    let secondInput = document.getElementById('secondPassword');
    let spanElement = document.createElement('span');
    spanElement.innerHTML = 'Нужно ввести одинаковые значения';
    spanElement.style.color = 'red';

    let input = document.getElementsByClassName('password');
    let btn = document.getElementById('btn');
    for (value of input) {
        if (input[0].value === input[1].value) {
            alert('You are welcome');
        } else {
            btn.before(spanElement);
        }
        return;
    }
}