class WhackAMoleGame {
    constructor() {
        this._difficulty = 0;
        this._playerCells = 0;
        this._computerCells = 0;
        this._arrayOfCells = [];
        this._counter = 0;
        this._levels = [['Easy', 1500], ['Medium', 1000], ['Hard', 500]];

        this.chooseDifficulty(this.startTheGame.bind(this));
    }

    startTheGame(difficultyLevel) {
        this.setDifficulty(difficultyLevel);
        this.createField();
        this.getCellsNodes();
        this.createArray();
        this.mixCells(this._arrayOfCells);
        this.createCells();
    }

    chooseDifficulty(startTheGame) {
        const buttonsContainer = document.createElement('div');
        buttonsContainer.classList.add('buttons-container');
        document.body.prepend(buttonsContainer);

        this._levels.map((el) => {
            buttonsContainer.insertAdjacentHTML('afterbegin', `<button id="${el[0]}">${el[0]}</button>`)
        });

        this.chooseDifficultyHandler(startTheGame, this._levels, buttonsContainer, this._difficulty);
    }

    chooseDifficultyHandler(startTheGame, difficultyName, buttonsContainer) {
        document.body.addEventListener('click', function speedListener(event) {
            let difficultyLevel = 0;
            let targetName = event.target.id;
            
            difficultyName.forEach(function (elem) {
                if (elem.includes(targetName)) {
                    difficultyLevel = targetName;
                    document.body.removeEventListener('click', speedListener);
                    startTheGame(difficultyLevel);
                    buttonsContainer.remove();
                }
            });
        });
    }

    clickListener() {
        this._noClick = setTimeout(this.giveCellToComputer.bind(this), this._difficulty);
        this._randomCell.addEventListener('click', () => {
            this.giveCellToPlayer();
            clearTimeout(this._noClick);
        }, {once: true});
    }

    setDifficulty(value) {
        this._levels.forEach(el => {
            if (value === el[0]) {
                this._difficulty = el[1];
            }
        });
    }

    createField() {
        for (let i = 0; i < 100; i++) {
            document.getElementById("fieldContainer")
                .appendChild(document.createElement("td"))
                .classList.add('default-cell');
        }

        if (!document.getElementById('fieldContainer')) {
            const fieldContainer = document.createElement('div');
            fieldContainer.id = 'fieldContainer';
            document.body.append(fieldContainer);
        }
    }

    getCellsNodes() {
        this._arrayOfCells = document.querySelectorAll('.default-cell');
    }

    createArray() {
        this._arrayOfCells = Array.from(this._arrayOfCells);
    }

    mixCells(array) {
        for (let i = array.length - 1; i > 0; i--) {
            let z = Math.floor(Math.random() * (i + 1));
            [array[i], array[z]] = [array[z], array[i]];
        }
    }

    createCells() {
        this.showScore();
        this.whoIsTheWinner();
        this._randomCell = this._arrayOfCells[this._counter++];
        
        if (this._randomCell) {
            this._randomCell.classList.add('blue-cell');
            this.clickListener();
            setTimeout(() => {
                this.createCells();
            }, this._difficulty);
        }
    }

    giveCellToPlayer() {
        this._randomCell.classList.remove('blue-cell');
        this._randomCell.classList.add('green-cell');
        this._playerCells++;
    }

    giveCellToComputer() {
        this._randomCell.classList.remove('blue-cell');
        this._randomCell.classList.add('red-cell');
        clearTimeout(this._noClick);
        this._computerCells++;
    }

    showScore() {
        if (document.querySelector('.score')) {
            document.querySelector('.score').remove();
        }

        const ul = document.createElement('ul');
        const playerScore = document.createElement('li');
        const computerScore = document.createElement('li');

        ul.classList.add('score');
        ul.id = 'score';

        playerScore.innerText = `Player score: ${this._playerCells}`;
        computerScore.innerText = `Computer score: ${this._computerCells}`;

        document.body.append(ul);
        ul.append(playerScore);
        ul.append(computerScore);
    }

    whoIsTheWinner() {
        if (this._playerCells === 50 || this._computerCells === 50) {
            const winnerName = (this._playerCells > this._computerCells) ? 'player' : 'computer';
            alert(`${winnerName} is the winner`);
            this.deleteGame();
            const game = new WhackAMoleGame;
        }
    }

    deleteGame() {
        document.getElementById('score').remove();
        document.getElementById('fieldContainer').remove();
        this._arrayOfCells = [];
    }
}

const game = new WhackAMoleGame;