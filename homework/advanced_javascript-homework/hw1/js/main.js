function Hamburger(size, ...stuffing) {
    if (!size) {
        new HamburgerException().noSizeHamburger();
    }

    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}

Hamburger.SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    name: 'STUFFING_CHEESE',
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    name: 'STUFFING_POTATO',
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'TOPPING_MAYO',
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    name: 'TOPPING_SPICE',
    price: 15,
    calories: 0
};


Hamburger.prototype.addTopping = function (topping) {
    if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
        new HamburgerException().wrongToppingFormat();
    }
    if (this.topping.includes(topping)) {
        new HamburgerException().duplicateTopping();
    }
    this.topping.push(topping);
};


Hamburger.prototype.removeTopping = function (topping) {
    if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
        new HamburgerException().wrongToppingFormat();
    }
    if (!(this.topping.includes(topping))) {
        new HamburgerException().invalidRemove();
    }
    this.topping.splice(this.topping.indexOf(topping), 1);
};


Hamburger.prototype.getToppings = function () {
    return this.topping;
};


Hamburger.prototype.getSize = function () {
    return this.size;
};


Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};


Hamburger.prototype.calculatePrice = function () {
    let hamburgerPrice = this.size.price;
    let stuffingPrice = 0;
    let stuffing = this.stuffing;
    for (let i = 0; i < stuffing.length; i++) {
        stuffingPrice += stuffing[i].price;
    }
    return hamburgerPrice + stuffingPrice;
};


Hamburger.prototype.calculateCalories = function () {
    let hamburgerCalories = this.size.calories;
    let stuffingCalories = 0;
    let stuffing = this.stuffing;
    for (let i = 0; i < stuffing.length; i++) {
        stuffingCalories += stuffing[i].calories;
    }
    return hamburgerCalories + stuffingCalories;
};


function HamburgerException() {
    this.noSizeHamburger = function () {
            throw new Error('No size given');
    }
    this.duplicateTopping = function () {
        throw new Error('You are trying to add the same topping');
    }
    this.wrongToppingFormat = function () {
        throw new Error('Wrong format of topping');
    }
    this.invalidRemove = function () {
        throw new Error('No topping in hamburger to remove');
    };
}


//new Hamburger
// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1