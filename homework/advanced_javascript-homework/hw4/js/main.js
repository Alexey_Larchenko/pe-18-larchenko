class Cards {
    constructor() {
        this.createContainer();
        this.createColumnButton();
        this.listenCards();
    }

    createContainer() {
        const container = document.createElement('div');
        container.classList.add('container');
        document.body.append(container);
    }

    createColumnButton() {
        const createColumnBtn = document.createElement('button');
        createColumnBtn.classList.add('column-btn');
        createColumnBtn.innerText = 'Добавить колонку';

        createColumnBtn.addEventListener('click', this.createColumn);

        document.body.append(createColumnBtn);
    }

    createColumn() {
        const cardContainer = document.createElement('div');
        cardContainer.classList.add('card-container');

        const createCardButton = document.createElement('button');
        createCardButton.innerText = 'Добавить карточку';
        createCardButton.classList.add('card-btn');

        const sortButton = document.createElement('button');
        sortButton.innerText = 'Отсортировать карточки';
        sortButton.classList.add('sort-btn');

        const column = document.createElement('div');
        column.classList.add('column');
        column.append(cardContainer);
        column.append(createCardButton);
        column.append(sortButton);

        const parentContainer = document.querySelector('.container');
        parentContainer.append(column);
    }

    listenCards() {
        const parentContainer = document.querySelector('.container');
        parentContainer.addEventListener('click', (event) => {
            if (event.target.classList.contains('card-btn')) {
                this.createCard(event.target);
            }
            if (event.target.classList.contains('sort-btn')) {
                this.sortCardsInColumn(event.target);
            }
        });
    }

    createCard(target, onDropCard, event) {
        const card = document.createElement('input');
        card.classList.add('card');

        if (onDropCard) {
            parent = target;
            card.innerHTML = event.dataTransfer.getData("text");
            parent.before(card);
        } else {
            parent = target.parentNode.firstElementChild;
            parent.append(card);
        }

        this.dragDrop(card);
    }

    sortCardsInColumn(event) {
        const cardsCollection = Array.from(event.parentNode.querySelectorAll('input'));
        const cardsValue = cardsCollection.map(elem => elem.value);
        const sortedCards = cardsValue.sort(function (a, b) {
            if (+a - +b)
                return a - b;
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0
        });
        for (let i = 0; i < cardsCollection.length; i++) {
            cardsCollection[i].value = sortedCards[i];
        }

    }

    dragDrop(card) {
        card.draggable = 'true';
        card.addEventListener('dragstart', this.onDragStart);
        card.addEventListener('drop', this.onDrop.bind(this));
        card.addEventListener('dragover', this.onDragOver);
    }

    onDragOver(event) {
        event.preventDefault();
    }

    onDragStart(event) {
        event.dataTransfer.setData("text", event.target.innerText);
        this.id = 'for-remove';
    }

    onDrop(event) {
        event.preventDefault();
        event.dataTransfer.getData("text");
        this.createCard(event.target, true, event);

        if (document.getElementById('for-remove')) {
            document.getElementById('for-remove').remove();
        }
    }
}

const card = new Cards();