async function filmList() {
    const {data} = await axios.get('https://swapi.dev/api/films/');
    const promiseArray = [];

    let id = 0;
    const result = data.results.map(({title, episode_id, opening_crawl}) => {
        const createdHtml = `
            <ul>
            <li><b>Название:</b> ${title}</li>
            <li><b>Номер эпизода:</b> ${episode_id}</li>
            <li><b>Содержание:</b> ${opening_crawl}</li>
            <ul id="${id}" class="second-ul">
            <b>Персонажи:</b>
            </ul>
            </ul>`;

        id++;
        return createdHtml;
    });

    document.querySelector('.container').insertAdjacentHTML('afterbegin', result.join(' '));

    for (let i = 0; i < data.results.length; i++) {
        data.results[i].characters.forEach(elem => promiseArray.push(axios.get(elem)));
        const promise = await Promise.all(promiseArray);
        promise.forEach(elem => document.getElementById(`${i}`).insertAdjacentHTML('beforeend', `<li>${elem.data.name}</li>`));
    }
}

filmList();