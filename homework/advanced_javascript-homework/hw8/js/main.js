const button = document.querySelector('.btn');
button.addEventListener('click', getUserIp);

async function getUserIp() {
    const response = await axios.get('https://api.ipify.org/?format=json');
    const {data} = await axios.get(`http://ip-api.com/json/${response.data.ip}?fields=continent,country,region,city,district&lang=ru`);

    document.body.innerHTML = `
    <ul>
        <li>Континент: ${data.continent}</li>
        <li>Страна: ${data.country}</li>
        <li>Регион: ${data.region}</li>
        <li>Город: ${data.city}</li>
        <li>Район: ${data.district}</li>
    </ul>`;
}
