const request = new XMLHttpRequest();

request.open('GET', 'https://swapi.dev/api/films/');
request.send();

request.onload = function () {
    if (request.readyState === 4 && request.status === 200) {
        const {results} = JSON.parse(request.response);

        for (let i = 0; i < results.length; i++) {
            results[i].characters.forEach(elem => {
                const req = new XMLHttpRequest();

                req.open('GET', elem);
                req.responseType = 'json';
                req.send();

                req.onload = function () {
                    if (req.readyState === 4 && req.status === 200) {
                        document.getElementById(`${i}`).insertAdjacentHTML('beforeEnd', `<li>${req.response.name}</li>`);
                    } else {
                        console.error(`Error ${request.status}: ${request.statusText}`);
                    }
                }
            });
        }

        let id = 0;
        const result = results.map(({title, episode_id, opening_crawl}) => {
            const createdHtml = `
            <ul>
            <li><b>Название:</b> ${title}</li>
            <li><b>Номер эпизода:</b> ${episode_id}</li>
            <li><b>Содержание:</b> ${opening_crawl}</li>
            <ul id="${id}" class="second-ul">
            <b>Персонажи:</b>
            </ul>
            </ul>`;

            id++;
            return createdHtml;
        });
        document.querySelector('.container').insertAdjacentHTML('beforeEnd', result.join(' '));
    } else {
        console.error(`Error ${request.status}: ${request.statusText}`);
    }
};