let objectOfPosts;
let objectOfUsers;

$.ajax({
    url: "https://jsonplaceholder.typicode.com/posts",
    method: "get",
    dataType: "json",
    success: data => {
        objectOfPosts = data;

        $.ajax({
            url: "https://jsonplaceholder.typicode.com/users",
            method: "get",
            dataType: "json",
            success: data => {
                objectOfUsers = data;
                createHTML();
            }
        });
    }
});

function createHTML() {
    const post = objectOfPosts.map(({title, body, userId, id}) => {
        let author;
        objectOfUsers.forEach(user => {
            if (user.id === userId) {
                author = user;
            }
        });

        function ucFirst(str) {
            if (!str) {
                return str;
            }
            return str[0].toUpperCase() + str.slice(1);
        }

        return `
        <div class="post" id="post${id}" data-id="${id}" data-user-id="${userId}">
            <h3 class="title">${ucFirst(title)}</h3>
            <p class="body">${ucFirst(body)}</p>
            <p class="description"> <strong>Name: </strong> ${author.name}</p>
            <p class="description"> <strong>Email: </strong> ${author.email}</p>
            <button class="delete">Delete</button>
            <button class="change">Change</button>
        </div>`;
    });

    document.querySelector('.container').insertAdjacentHTML('beforeend', post.join(' '));

    document.querySelectorAll('.delete').forEach(element => element.addEventListener('click', deleteThePost.bind(this)));
    document.querySelectorAll('.change').forEach(element => element.addEventListener('click', changeThePost.bind(this)));
}

function deleteThePost(event) {
    const postId = event.target.parentNode.getAttribute('data-id');
    $.ajax({
        url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
        method: "DELETE"
    });

    event.target.parentNode.remove();
}

function changeThePost({target}) {
    const title = target.parentNode.querySelector('.title').innerHTML;
    const body = target.parentNode.querySelector('.body').innerHTML;
    const modal = document.createElement('form');
    const changeButton = document.createElement('input');

    changeButton.value = 'Change';
    changeButton.type = 'submit';

    modal.setAttribute('class', 'change-modal');
    modal.setAttribute('data-id', `${target.parentNode.dataset.id}`);
    modal.setAttribute('data-user-id', `${target.parentNode.dataset.userId}`);

    modal.innerHTML = `
    <textarea id="change-title" name="title">${title}</textarea>
    <textarea id="change-body" name="body">${body}</textarea>`;

    modal.addEventListener('submit', submitChange);
    modal.append(changeButton);

    document.body.append(modal);
}

function submitChange(event) {
    event.preventDefault();

    const {currentTarget} = event;
    const body = {
        id: `${currentTarget.dataset.id}`,
        title: `${currentTarget.querySelector('[name="title"]').value}`,
        body: `${currentTarget.querySelector('[name="body"]').value}`,
        userId: `${currentTarget.dataset.userId}`,
    };

    $.ajax({
        url: `https://jsonplaceholder.typicode.com/posts/${currentTarget.dataset.id}`,
        method: "PUT",
        dataType: "json",
        data: JSON.stringify(body),
        success: () => {
            document.body.querySelector(`#post${currentTarget.dataset.id}`).querySelector('.title').innerHTML = body.title;
            document.body.querySelector(`#post${currentTarget.dataset.id}`).querySelector('.body').innerHTML = body.body;
            currentTarget.remove();
        }
    });
}