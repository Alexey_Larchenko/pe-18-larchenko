const contactButton = document.querySelector('.btn-black');
contactButton.addEventListener('click', () => {
    setCookies('experiment', 'novalue', {'max-age': 300});

    if(getCookie('new-user')){
        setCookies('new-user', 'false')
    } else {
        setCookies('new-user', 'true')
    }
});

function setCookies(name, value, options = {}) {
    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
    options = {path: '/', ...options};

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    for (let key in options) {
        updatedCookie += "; " + key;
        let value = options[key];
        if (value !== true) {
            updatedCookie += "=" + value;
        }
    }

    document.cookie = updatedCookie;
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}