class Hamburger {
    constructor(size, ...stuffing) {
        if (!size) {
            this.HamburgerException('No size given');
        }
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }

    addTopping(topping) {
        if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
            this.HamburgerException('Wrong format of topping');
        }
        if (this.topping.includes(topping)) {
            this.HamburgerException('You are trying to add the same topping');
        }
        this.topping.push(topping);
    }

    removeTopping(topping) {
        if (!(topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE)) {
            this.HamburgerException('Wrong format of topping');
        }
        if (!(this.topping.includes(topping))) {
            this.HamburgerException('No topping in hamburger to remove')
        }
        this.topping.splice(this.topping.indexOf(topping), 1);
    }

    getToppings() {
        return this.topping;
    }

    getSize() {
        return this.size;
    }

    getStuffing() {
        return this.stuffing;
    }

    calculatePrice() {
        let hamburgerPrice = this.size.price;
        let stuffingPrice = 0;
        let stuffing = this.stuffing;
        for (let i = 0; i < stuffing.length; i++) {
            stuffingPrice += stuffing[i].price;
        }
        return hamburgerPrice + stuffingPrice;
    }

    calculateCalories() {
        let hamburgerCalories = this.size.calories;
        let stuffingCalories = 0;
        let stuffing = this.stuffing;
        for (let i = 0; i < stuffing.length; i++) {
            stuffingCalories += stuffing[i].calories;
        }
        return hamburgerCalories + stuffingCalories;
    }

    HamburgerException(errorMessage) {
        throw new Error(errorMessage);
    }
}

Hamburger.SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    name: 'STUFFING_CHEESE',
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    name: 'STUFFING_POTATO',
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'TOPPING_MAYO',
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    name: 'TOPPING_SPICE',
    price: 15,
    calories: 0
};

//new Hamburger
// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1