let patient = {
    firstName: 'John',
    lastName: 'Wick',
    dateOfBirth: "02.09.1964",
    visits: [],
    addVisit: function (date) {
        date = Object.defineProperty(patient, 'visits', {
            value: [date],
            configurable: true,
            writable: false,
            enumerable: true
        });
    },
    getLastVisitDays: function () {
        let nowDate = new Date();
        patient.visits = new Date(2020, 1, 12);
        let daysVisit = nowDate - patient.visits;
        console.log(daysVisit);
    }
};

console.log(patient.addVisit("12.01.2020"));
console.log(patient);
