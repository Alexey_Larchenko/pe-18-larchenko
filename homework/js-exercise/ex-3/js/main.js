const patientVisits = [
    {
        date: "13.07.2019",
        time: 25,
        diagnosis: "Лишний вес"
    },
    {
        date: "25.08.2019",
        time: 45,
        diagnosis: "Лишний вес. Тахикардия"
    },
    {
        date: "03.09.2019",
        time: 15,
        diagnosis: "Лишний вес. Тахикардия."
    },
    {
        date: "26.09.2019",
        time: 35,
        diagnosis: "Лишний вес. Тахикардия. Стенокардия."
    },
    {
        date: "26.01.2020",
        time: 35,
        diagnosis: "Лишний вес. Тахикардия. Стенокардия."
    },
];

const longVisits = [];
for (let i = 0; i < patientVisits.length; i++) {
    if (patientVisits[i].time > 30) {
        longVisits.push(patientVisits[i]);
    }
}
console.log(longVisits);