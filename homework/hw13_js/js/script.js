let themeNumber = 1;
let defaultTheme = 1;
let ext = 'css';

function getUrl(num) {
    return ext + '/theme-' + num + '.css';
}

function getThemeNum() {
    themeNumber = themeNumber % 2 === 0 ? 1 : 2;
    return themeNumber;
}

function init() {
    let link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('href', getUrl(defaultTheme));
    document.head.appendChild(link);
}

function changeTheme() {
    document.head.querySelector('link').setAttribute('href', getUrl(getThemeNum()));
}

init();