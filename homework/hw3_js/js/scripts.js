function calculator() {
    const num1 = +prompt('Введите первое число');
    const num2 = +prompt('Введите второе число');
    const operation = prompt('Введите математическую операцию', '+');

    if (operation === '+') {
        console.log(num1 + num2);
    } else if (operation === '-') {
        console.log(num1 - num2);
    } else if (operation === '*') {
        console.log(num1 * num2);
    } else if (operation === '/') {
        console.log(num1 / num2);
    }
}

calculator();